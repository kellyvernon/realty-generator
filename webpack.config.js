const path = require('path');
const webpack = require('webpack');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const compilerOptions = require('./tsconfig.json').compilerOptions;
const modes = {
  dev: 'development',
  none: 'none',
};

module.exports = {
  entry: './src/index.tsx',
  infrastructureLogging: {
    debug: true,
    level: "verbose"
  },
  externals: {
    'fs': 'require(\'fs\')',
    'path': 'require(\'path\')'
  },
  mode: modes.dev,
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, compilerOptions.outDir)
  },
  plugins: [
    new webpack.DefinePlugin({
      APP_VERSION: JSON.stringify(require('./package.json').version)
    }),
    new CopyWebpackPlugin({
      patterns: [
        {from: 'static'}
      ]
    }),
    new MiniCssExtractPlugin()
  ],
  watchOptions: {
    ignored: ['node_modules']
  },
  devServer: {
    contentBase: compilerOptions.outDir,
    historyApiFallback: true
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: 'ts-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          'file-loader',
        ],
      },
    ]
  },
  target: 'web',
  resolve: {
    extensions: ['.ts', '.tsx', '.js']
  }
};
