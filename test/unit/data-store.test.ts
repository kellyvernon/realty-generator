'use strict';

import {DataStore} from '../../src/data-store';

describe('data store tests', () => {
  describe('and getting collection and saving collection', () => {
    it('should call save', () => {
      const object = {'foo': 'bar'};

      const dataStore = new DataStore();

      dataStore.save('some', object);
      const actual = dataStore.get('some');

      expect(actual).toEqual(object);
    });
  });
});
