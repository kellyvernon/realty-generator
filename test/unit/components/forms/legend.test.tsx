import React from 'react';
import {create} from 'react-test-renderer';
import {Legend} from '../../../../src/components/forms/legend';

describe('Legend tests', () => {
  it('should be there', () => {
    const app = create(<Legend/>);
    expect(app.toJSON()).toMatchSnapshot();
  });
});
