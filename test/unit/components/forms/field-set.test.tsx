import React from 'react';
import {create} from 'react-test-renderer';
import {Fieldset} from '../../../../src/components/forms/field-set';

describe('Fieldset tests', () => {
  it('should be there', () => {
    const app = create(<Fieldset/>);
    expect(app.toJSON()).toMatchSnapshot();
  });
});
