import React from 'react';
import {create} from 'react-test-renderer';
import {RangeValidationInput} from '../../../../src/components/forms/range-validation-input';

describe('RangeValidationInput tests', () => {
  it('should be there', () => {
    // tslint:disable-next-line:no-empty
    const onChange = () => {
    };
    const app = create(<RangeValidationInput title={'goal'}
                                             max={2000}
                                             min={500}
                                             prefix={'pre'}
                                             suffix={'suf'}
                                             name={'a-chk'}
                                             value={1000}
                                             onChange={onChange}/>);
    expect(app.toJSON()).toMatchSnapshot();
  });
});
