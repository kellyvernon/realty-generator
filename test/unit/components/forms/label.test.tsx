import React from 'react';
import {create} from 'react-test-renderer';
import {Label} from '../../../../src/components/forms/label';

describe('Label tests', () => {
  it('should be there', () => {
    const app = create(<Label/>);
    expect(app.toJSON()).toMatchSnapshot();
  });
});
