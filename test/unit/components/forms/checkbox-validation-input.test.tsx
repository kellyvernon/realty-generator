import React from 'react';
import {create} from 'react-test-renderer';
import {CheckboxValidationInput} from '../../../../src/components/forms/checkbox-validation-input';

describe('CheckboxValidationInput tests', () => {
  it('should be there', () => {
    // tslint:disable-next-line:no-empty
    const onChange = () => {};
    const app = create(<CheckboxValidationInput title={'checker'}
                                                name={'a-chk'}
                                                value={false}
                                                onChange={onChange}/>);
    expect(app.toJSON()).toMatchSnapshot();
  });
});
