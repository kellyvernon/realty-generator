import React from 'react';
import {create} from 'react-test-renderer';
import {Form} from '../../../../src/components/forms/form';

describe('Form tests', () => {
  it('should be there', () => {
    const app = create(<Form/>);
    expect(app.toJSON()).toMatchSnapshot();
  });
});
