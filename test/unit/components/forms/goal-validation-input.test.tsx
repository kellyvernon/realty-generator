import React from 'react';
import {create} from 'react-test-renderer';
import {GoalValidationInput} from '../../../../src/components/forms/goal-validation-input';

describe('GoalValidationInput tests', () => {
  it('should be there', () => {
    // tslint:disable-next-line:no-empty
    const onChange = () => {
    };
    const app = create(<GoalValidationInput title={'goal'}
                                            max={2000}
                                            min={500}
                                            prefix={'pre'}
                                            suffix={'suf'}
                                            name={'a-chk'}
                                            value={1000}
                                            onChange={onChange}/>);
    expect(app.toJSON()).toMatchSnapshot();
  });
});
