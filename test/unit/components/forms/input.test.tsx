import React from 'react';
import {create} from 'react-test-renderer';
import {Input} from '../../../../src/components/forms/input';

describe('Input tests', () => {
  it('should be there', () => {
    const app = create(<Input/>);
    expect(app.toJSON()).toMatchSnapshot();
  });
});
