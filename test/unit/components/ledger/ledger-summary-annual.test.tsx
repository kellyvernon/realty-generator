import React from 'react';
import {create} from 'react-test-renderer';
import {LedgerSummaryAnnual} from '../../../../src/components/ledger/ledger-summary-annual';
import {Ledger} from 'realty-generator-core';

describe('LedgerSummaryAnnual tests', () => {
  it('should be there', () => {
    const app = create(<LedgerSummaryAnnual
      goal={2000} ledger={new Ledger()}
      data={{
        equity: 50,
        cashFlow: 20,
        averageCashFlow: 200,
        purchases: 0,
        balance: 2000,
        date: new Date(Date.now())
      }}/>);
    expect(app.toJSON()).toMatchSnapshot();
  });
});
