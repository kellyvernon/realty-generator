import React from 'react';
import {create} from 'react-test-renderer';
import {Ledger} from 'realty-generator-core';
import {LedgerSummaryMonthly} from '../../../../src/components/ledger/ledger-summary-monthly';

describe('LedgerSummaryMonthly tests', () => {
  it('should be there', () => {
    const app = create(<LedgerSummaryMonthly
      ledger={new Ledger()}
      data={{
        equity: 50,
        cashFlow: 20,
        averageCashFlow: 200,
        purchases: 0,
        balance: 2000,
        date: new Date(Date.now())
      }}/>);
    expect(app.toJSON()).toMatchSnapshot();
  });
});
