import React from 'react';
import {create} from 'react-test-renderer';
import {LedgerSummaryTransaction} from '../../../../src/components/ledger/ledger-summary-transaction';

describe('LedgerSummaryTransaction tests', () => {
  it('should be there', () => {
    const app = create(<LedgerSummaryTransaction
      />);
    expect(app.toJSON()).toMatchSnapshot();
  });
});
