
import {Application} from '../../src/application';

import React from 'react';
import {create} from 'react-test-renderer';

describe('Application tests', () => {
  it('should be there', () => {
    const app = create(<Application/>);
    expect(app.toJSON()).toMatchSnapshot();
  });
});
