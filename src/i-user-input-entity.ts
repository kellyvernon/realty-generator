export interface IUserInputEntity {
    monthlyIncomeAmountGoal: number;
    monthlySavedAmount: number;
    currentlySavedAmount: number;
    minMonthlyCashOnCashAmountHouse: number;
    minMonthlyCashOnCashAmountApartment: number;
    minEquityCapturePercent: number;
    minInvestAmount: number;
    maxInvestAmount: number;
    isValid: boolean;
}
