import {IUserInputEntity} from '../i-user-input-entity';

export const userInputDefault: IUserInputEntity = {
  monthlyIncomeAmountGoal: 3000,
  monthlySavedAmount: 100,
  currentlySavedAmount: 3000,
  minMonthlyCashOnCashAmountHouse: 300,
  minMonthlyCashOnCashAmountApartment: 275,
  minEquityCapturePercent: 30,
  minInvestAmount: 600,
  maxInvestAmount: 25000,
  isValid: false
}
