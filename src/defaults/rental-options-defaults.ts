import {IRentalGeneratorOptionEntity} from '../i-rental-generator-option-entity';
import {RentalTypes} from 'realty-generator-core';

export const rentalOptionRentalDefaults: IRentalGeneratorOptionEntity = {
  type: RentalTypes.Rental,
  renewalInMonths: 0,
  maxRentalOpportunities: 0,
  lowestPriceDown: 0,
  highestPriceDown: 0,
  lowestSellPercent: 0,
  highestSellPercent: 0,
  lowestMinSellInYears: 0,
  highestMinSellInYears: 0,
  lowestCashOnCashPercent: 0,
  highestCashOnCashPercent: 0,
  minRentalOpportunities: 0,
  lowestRefinancePercent: 0,
  highestRefinancePercent: 0,
  lowestMinRefinanceInMonths: 0,
  highestMinRefinanceInMonths: 0,
  isEnabled: true,
  isValid: false
}

export const rentalOptionSingleFamilyDefaults: IRentalGeneratorOptionEntity = {
  type: RentalTypes.SingleFamily,
  renewalInMonths: 3,
  maxRentalOpportunities: 10,
  lowestPriceDown: 100000,
  highestPriceDown: 150000,
  lowestSellPercent: 10,
  highestSellPercent: 25,
  lowestMinSellInYears: 1,
  highestMinSellInYears: 2,
  lowestCashOnCashPercent: 10,
  highestCashOnCashPercent: 20,
  minRentalOpportunities: 0,
  lowestRefinancePercent: 10,
  highestRefinancePercent: 20,
  lowestMinRefinanceInMonths: 2,
  highestMinRefinanceInMonths: 4,
  isEnabled: true,
  isValid: false
}

export const rentalOptionPassiveApartmentDefaults: IRentalGeneratorOptionEntity = {
  type: RentalTypes.PassiveApartment,
  renewalInMonths: 3,
  maxRentalOpportunities: 7,
  lowestPriceDown: 0,
  highestPriceDown: 0,
  lowestSellPercent: 150,
  highestSellPercent: 190,
  lowestMinSellInYears: 5,
  highestMinSellInYears: 8,
  lowestCashOnCashPercent: 8,
  highestCashOnCashPercent: 10,
  minRentalOpportunities: 0,
  lowestRefinancePercent: 40,
  highestRefinancePercent: 60,
  lowestMinRefinanceInMonths: 36,
  highestMinRefinanceInMonths: 40,
  isEnabled: true,
  isValid: false
}

export const rentalOptionsDefaults: IRentalGeneratorOptionEntity[] = [
  rentalOptionSingleFamilyDefaults,
  rentalOptionPassiveApartmentDefaults
]
