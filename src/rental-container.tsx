import React from 'react';
import {RentalGeneratorOptions} from './rental-generator-options';
import {PageTitle} from './components/page-title';
import styled from 'styled-components';
import {INavHandleResponse, NavigationSub} from './components/navigation-sub';
import {RentalTypes} from 'realty-generator-core';
import {IRentalGeneratorOptionEntity} from './i-rental-generator-option-entity';
import {rentalOptionsDefaults} from './defaults/rental-options-defaults';

export interface IRentalContainerState extends IRentalContainerTabState {
  doLoad: boolean;
}

export interface IRentalContainerTabState {
  activeTab: {
    type: RentalTypes,
    name: string,
    index: number
  }
}

export interface IRentalContainerResult {
  options: IRentalGeneratorOptionEntity[];
  isValid: boolean;
}

export interface IRentalContainerProps {
  id?: string;
  options: IRentalGeneratorOptionEntity[];
  handleChange: (result: IRentalContainerResult) => void;
}

const DivWrap = styled.div`
  display: inline-block;
`;

const tabNames = [{
  name: 'Passive Multi Family',
  type: RentalTypes.PassiveApartment
}, {
  name: 'Single Family',
  type: RentalTypes.SingleFamily
}];

/**
 * TODO: currently, this doesn't perform the load validation check when toggled between one rental type or another.
 */
export class RentalContainer extends React.Component<IRentalContainerProps, IRentalContainerState> {
  static defaultProps = {
    options: rentalOptionsDefaults
  }

  constructor(props) {
    super(props);
    this.handleOptionsChange = this.handleOptionsChange.bind(this);
    this.handleSubNavClick = this.handleSubNavClick.bind(this);

    this.state = {
      doLoad: true,
      activeTab: {
        name: tabNames[0].name,
        type: tabNames[0].type,
        index: 0
      }
    };
  }

  componentDidMount() {
    if (this.state.doLoad) {
      this.setState({doLoad: false}, () => {
        this.props.handleChange({
          options: this.props.options,
          isValid: this.props.options.every(x => !x.isEnabled) ? false : this.props.options.every(x => x.isValid)
        });
      });
    }
  }

  componentWillUnmount() {
    this.setState({doLoad: true});
  }

  handleOptionsChange(result) {
    this.update(result);
  }

  getForState(active: INavHandleResponse): IRentalContainerTabState {
    return {
      activeTab: {
        ...active,
        type: tabNames.find(u => u.name === active.name).type
      }
    };
  }

  handleSubNavClick(navEvent: INavHandleResponse) {
    const tabInfo = tabNames.find(t => t.name === navEvent.name);
    const dataOptions = this.props.options.find(t => t.type === tabInfo.type);

    if (!dataOptions) {
      this.createNew(tabInfo.type);
    } else {
      this.update(dataOptions);
    }
    this.setState(this.getForState(navEvent), () => {
      this.props.handleChange({
        options: this.props.options,
        isValid: this.props.options.every(x => !x.isEnabled) ? false : this.props.options.every(x => x.isValid)
      });
    });
  }

  createNew(type) {
    this.update({
      type,
      renewalInMonths: 0,
      maxRentalOpportunities: 0,
      lowestPriceDown: 0,
      highestPriceDown: 0,
      lowestSellPrice: 0,
      highestSellPrice: 0,
      lowestMinSellInYears: 0,
      highestMinSellInYears: 0,
      lowestCashOnCashPercent: 0,
      highestCashOnCashPercent: 0,
      lowestInvestmentPercent: 0,
      highestInvestmentPercent: 0,
      isEnabled: true,
      isValid: false
    });
  }

  update(item) {
    const existingNonMatchingOptionType = this.props.options.filter(e => e.type !== item.type);

    existingNonMatchingOptionType.push(item);

    this.props.handleChange({
      options: existingNonMatchingOptionType,
      isValid: existingNonMatchingOptionType.every(x => !x.isEnabled) ? false : existingNonMatchingOptionType.every(x => x.isValid)
    });
  }

  render() {
    const rentalOption = this.props.options.find(e => e.type === this.state.activeTab.type);
    return <DivWrap>
      <div>
        <PageTitle title='Rental Options'/>
      </div>
      <NavigationSub handleClick={this.handleSubNavClick.bind(this)}
                     activeTab={this.state.activeTab.index}
                     tabs={tabNames.map(v => v.name)}
                     height={50}
                     areActive={tabNames.map(() => true)}/>
      <RentalGeneratorOptions data={rentalOption}
                              key={this.state.activeTab.type}
                              handleChange={this.handleOptionsChange.bind(this)}/>
    </DivWrap>;
  }
}
