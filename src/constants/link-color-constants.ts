export enum LinkColorConstants {
  highlight = '#2693ff',
  highlightBlue = '#0d8aff',
  disabled = '#a0a0a0'
}
// @ts-ignore
const color = '#007ae6';

export enum PageColors {
  backgroundColor = '#1d1d26'
}


