import React from 'react';
import {mainTabConstants} from './constants/main-tab-constants';
import {Button} from './components/button';
import {DivFlexContainer} from './components/div-flex-container';
import {INavigationResponse} from './components/i-navigation-response';

export interface IGetStartedProps {
  handleClick: (response: INavigationResponse) => void;
}

export const GetStarted: React.FunctionComponent<IGetStartedProps> = ({handleClick}) => {

  const buttonClick = (tabName: string) => {
    handleClick({
      tabName
    });
  };

  return <DivFlexContainer id='screen-one' className='text-center get-started'>
    <Button id={'get-started-button'} className={'mx-auto'}
            onClick={buttonClick.bind(this, mainTabConstants.userInfo)}>Get Started</Button>
  </DivFlexContainer>;
};
