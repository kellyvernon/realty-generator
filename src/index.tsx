import React from 'react';
import * as ReactDOM from 'react-dom';
import {Application} from './application';

declare var APP_VERSION: string;


ReactDOM.render(<Application/>, document.getElementById('root'));
document.getElementById('app-version').innerText = APP_VERSION;

