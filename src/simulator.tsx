import React from 'react';
import {Charts} from './components/charts/charts';
import styled from 'styled-components';
import {simulateTabsConstants} from './constants/simulate-tab-constants';
import {
  fixedAndFloat,
  Generator,
  ICreateOptions,
  IGeneratorOptions,
  IRentalGeneratorOptions,
  Ledger,
  LedgerItem,
  LedgerItemType,
  OptionsTranslator,
  randomNumberBetween,
  Rental,
  RentalCollection,
  RentalGenerator,
  RentalTypes,
  User,
  ValueCache,
} from 'realty-generator-core';
import {INavHandleResponse, NavigationSub} from './components/navigation-sub';
import {PageTitle} from './components/page-title';
import {LedgerMain} from './components/ledger/ledger-main';
import {IRentalGeneratorOptionEntity} from './i-rental-generator-option-entity';
import {IUserInputEntity} from './i-user-input-entity';
import {IGeneratorTyping} from 'realty-generator-core/dist/src/generators/i-generator-typing';

const tabNames = [{
  name: simulateTabsConstants.ledger
}, {
  name: simulateTabsConstants.charts
}, {
  name: simulateTabsConstants.data
}];

const navHeight = 96;

const DivWrap = styled.div`
  display: inline-block;
`;

const AdjustTopDiv = styled.div`
  margin-top: ${navHeight}px;
`;

interface IOptions {
  rentalType: RentalTypes;
  options: IRentalGeneratorOptions;
  generator: RentalGenerator<Rental>;
}

export class Simulator extends React.Component<ISimulatorProps, ISimulatorState> {
  constructor(props) {
    super(props);
    this.handleTabClick = this.handleTabClick.bind(this);

    this.state = {
      activeTab: {
        name: tabNames[0].name,
        index: 0
      }
    };
  }

  /**
   * @param navEvent {{name: string, index:number}}
   */
  handleTabClick(navEvent: INavHandleResponse) {
    this.setState({
      activeTab: {
        ...navEvent
      }
    });
  }

  generate() {
    const currentFull = new Date(Date.now());
    const optionsToGenerators: IOptions[] = this.props.value.rentalOptionsInput.filter(p => p.isEnabled && p.isValid).map(o => {
      let translate: OptionsTranslator;
      if (o.type === RentalTypes.PassiveApartment) {
        translate = (options: ICreateOptions) => {
          return {
            ...options,
            cashOnCashPercent: randomNumberBetween(o.lowestCashOnCashPercent, o.highestCashOnCashPercent),
            investmentAmounts: [50000, 100000, 150000, 200000, 250000, 500000].filter(e => {
              const highestAmountWithLoanAndClosingCostsAndRepairs = o.highestPriceDown / 3.333;
              return e <= highestAmountWithLoanAndClosingCostsAndRepairs;
            })
          };
        };
      }

      if (o.type === RentalTypes.SingleFamily) {
        translate = (options: ICreateOptions) => {
          const investedAmount = randomNumberBetween(20000, 40000);
          const investmentPercent = investedAmount / options.purchasePrice * 100;
          const randomCashOnCashPercent = randomNumberBetween(o.lowestCashOnCashPercent, o.highestCashOnCashPercent);
          const annualCashFlow = investedAmount * randomCashOnCashPercent / 100;

          return {
            ...options,
            investmentPercent,
            cashOnCashAmountMonthly: fixedAndFloat(annualCashFlow / 12)
          };
        };
      }

      const oFinal: IRentalGeneratorOptions = o;
      oFinal.translate = translate;
      return {
        rentalType: o.type,
        options: oFinal,
        generator: new RentalGenerator<Rental>(new ValueCache(new Date(Date.UTC(
          currentFull.getUTCFullYear(),
          currentFull.getUTCMonth(),
          1)), []))
      };
    });

    const user = new User(this.props.value.userInput);
    user.rentals = new RentalCollection();
    user.ledger = new Ledger();

    if (this.props.value.userInput.currentlySavedAmount > 0) {
      const date = new Date(Date.now());
      user.ledger.add(new LedgerItem({
        type: LedgerItemType.Income,
        amount: this.props.value.userInput.currentlySavedAmount,
        created: new Date(date.getUTCFullYear(), date.getUTCMonth(), 1),
        note: 'savings'
      }));
    }

    const rentalCacheCollection: IGeneratorTyping[] = optionsToGenerators.map(o => ({
      rentalType: o.rentalType,
      generator: o.generator
    }));

    const rentalOptions: IGeneratorOptions[] = optionsToGenerators.map(o => ({
      rentalType: o.rentalType,
      options: o.options
    }));

    const gen = new Generator(rentalCacheCollection);
    return gen.simulate(user, rentalOptions, 15);
  }

  render() {
    const timeline = this.generate();

    return <DivWrap>
      <div>
        <PageTitle title='Simulation'/>
      </div>
      <NavigationSub handleClick={this.handleTabClick.bind(this)}
                     activeTab={this.state.activeTab.index}
                     tabs={tabNames.map(v => v.name)}
                     height={50}
                     areActive={tabNames.map(() => true)}/>
      <AdjustTopDiv/>
      {/*
        <div> Started: {moment(timeline.user.ledger.first().created).format('MMM, YYYY')} and Ended: {moment(timeline.date).format('MMM, YYYY')}</div>
        <div> Your Goal: {currencyFormatter(timeline.user.monthlyIncomeAmountGoal)}</div>
        <div> Monthly Cash flow: {currencyFormatter(timeline.user.rentals.cashOnCashAmountMonthly(timeline.date))}</div>
        */}

      {
        this.state.activeTab.name === simulateTabsConstants.ledger &&
        <LedgerMain ledger={timeline.user.ledger} goal={timeline.user.monthlyIncomeAmountGoal}/>
      }
      {
        this.state.activeTab.name === simulateTabsConstants.charts &&
        <Charts ledger={(timeline.user.ledger as Ledger)} rentalCollection={timeline.user.rentals}/>
      }
      {
        this.state.activeTab.name === simulateTabsConstants.data && <pre>
          {JSON.stringify(timeline, null, '\t')}
          </pre>
      }
    </DivWrap>;
  }
}

export interface ISimulatorState {
  activeTab: INavHandleResponse;
}

export interface ISimulatorProps {
  value: {
    userInput: IUserInputEntity,
    rentalOptionsInput: IRentalGeneratorOptionEntity[]
  }
}
