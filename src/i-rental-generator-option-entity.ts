import {RentalTypes} from 'realty-generator-core';

export interface IRentalGeneratorOptionEntity {
  id?: string;
  /**
   * the type of property values you area editing
   */
  type: RentalTypes;
  /**
   * used to allow editing in the UI
   */
  isEnabled: boolean;
  renewalInMonths: number;
  minRentalOpportunities: number;
  maxRentalOpportunities: number;
  lowestPriceDown: number;
  highestPriceDown: number;
  lowestSellPercent: number;
  highestSellPercent: number;
  lowestMinSellInYears: number;
  highestMinSellInYears: number;
  lowestCashOnCashPercent: number;
  highestCashOnCashPercent: number;
  lowestRefinancePercent: number;
  highestRefinancePercent: number;
  lowestMinRefinanceInMonths: number;
  highestMinRefinanceInMonths: number;
  isValid: boolean;
}
