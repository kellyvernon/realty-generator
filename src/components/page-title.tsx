import styled from 'styled-components';
import React from 'react';

export const H2Pretty = styled.h2`
  background-image: url('images/title-page-background.png');
  width: 960px;
  height: 75px;
  padding: 1.5em 1em 1em;
  margin: 0;
  color: black;
`;

export interface IPageTitleProps {
  title: string;
}

export const PageTitle: React.FunctionComponent<IPageTitleProps> = (props) =>
  <H2Pretty>
    {props.title}
  </H2Pretty>
