import styled from 'styled-components';
import {PageColors} from '../constants/link-color-constants';

export interface INavProps {
  paddingTop?: string;
  marginTop?: string;
}

export const Nav = styled.ul<INavProps>`
  display: -webkit-flex;
  display: flex;
  justify-content: space-evenly;
  list-style-type: none;
  padding: ${props => props.paddingTop || '1em'} 1em 1em 1em;
  background-color: ${PageColors.backgroundColor};
  ${props => props.marginTop
  ? `margin: ${props.marginTop} 0 0 0;`
  : 'margin: 0;'}
`;


export interface INavListItemProps {
  paddingRight?: string;
}

export const NavListItem = styled.li<INavListItemProps>`
  padding-right: ${props => props.paddingRight ? props.paddingRight : '15em'};
  padding-left:0;

  &:last-child {
    padding-right: 1em;
  }
`;
