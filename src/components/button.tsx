import styled from 'styled-components';

export interface IButtonProps {
  highlight?: string;
  disabled?: string;
  color?: string;
}

export const Button = styled.button<IButtonProps>`
  background-color: ${props => props.color || '#007ae6'};
  border-radius: .2em;
  border-style: none;
  margin: 0 1em;
  padding: 0.25em 1em;
  color: black;
  white-space:nowrap;

  &:hover {
    cursor: pointer;
    background-color: ${props => props.highlight || '#2693ff'};
    color: white;
  }
  &:disabled {
    background-color: ${props => props.disabled || '#1b5085'};
    cursor: default;
    color: black;
  }
`;
