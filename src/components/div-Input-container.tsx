import styled from 'styled-components';
import {
  DivFlexContainer,
  DivFlexContainerAlignItems,
  DivFlexContainerDirections,
  DivFlexContainerJustifyContent,
  IDivFlexContainerProps
} from './div-flex-container';

export interface IDivInputContainer extends IDivFlexContainerProps {
  width?: string;
  backgroundColor?: string;
}

export const DivInputContainer = styled(DivFlexContainer).attrs<IDivInputContainer>(p => ({
  width: p.width ? p.width : '875px',
  direction: p.direction ? p.direction : DivFlexContainerDirections.COLUMN,
  alignItems: p.alignItems ? p.alignItems : DivFlexContainerAlignItems.STRETCH,
  justifyContent: p.justifyContent ? p.justifyContent : DivFlexContainerJustifyContent.FLEX_START,
}))<IDivInputContainer>`
  width: ${props => props.width};
  ${props => !props.backgroundColor ? '' : `background:${props.backgroundColor};`}
`;
