import styled from 'styled-components';
import {Nav, NavListItem} from './navigation';
import React, {Component} from 'react';

import {DivFlexContainerJustifyContent} from './div-flex-container';
import {LinkColorConstants} from '../constants/link-color-constants';

const NavMainUl = styled(Nav)<{ justifyContent?: DivFlexContainerJustifyContent }>`
  justify-content: ${props => props.justifyContent || DivFlexContainerJustifyContent.FLEX_START};
  padding: 0;
  background: url('images/sub-menu-background.jpg');
  height: 50px;
`;

const NavListItemAdjust = styled(NavListItem)`
  display: -webkit-flex;
  display: flex;
  align-items: center;

  width: 220px;
  padding-left: 2px;
  &:first-child{
      margin-left: 40px;
  }
`;

const color = '#007ae6';
const backgroundColor = '#1d1d26';

const NavSubBase = styled.a`
  display: -webkit-flex;
  display: flex;
  width: 220px;
  height: auto;
  line-height:30px;
  font-weight: normal;
  font-stretch: normal;
  justify-content: center;
`;

const transition = 'border-bottom .3s cubic-bezier(0, 1.11, 1, 1); padding-top .3s cubic-bezier(0, 1.11, 1, 1); border-bottom .3s cubic-bezier(0, 1.11, 1, 1); ';

export const NavSubLinkEnabled = styled(NavSubBase)`
  -webkit-transition: ${transition};
  -moz-transition: ${transition};
  -o-transition: ${transition};
  transition: ${transition};

  color: black;
  border-bottom: 5px solid ${color};
  padding: 15px 0 0;

  &:hover {
    cursor: pointer;
    color: black;
    border-bottom: 10px solid ${LinkColorConstants.highlight};
    padding-top: 10px;
  }
`;

export const NavSubLinkActive = styled(NavSubBase)<{ active: boolean }>`
  background-color: ${backgroundColor};
  color: ${LinkColorConstants.highlight};
  border-top: 10px solid ${LinkColorConstants.highlight};
  padding-bottom: 10px;
`;

export interface INavHandleResponse {
  name: string;
  index: number;
}

export interface INavigationSubProps {
  tabs: string[];
  handleClick: (e: INavHandleResponse) => void;
  activeTab: number;
  height: number;
  areActive: boolean[];
}

export class NavigationSub extends Component<INavigationSubProps> {
  static defaultProps = {
    height: 0,
    tabs: [],
    activeTab: 0
  }

  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  /**
   *
   * @param entity
   * @returns {{name: string, index:number}}
   */
  handleClick(entity) {
    this.props.handleClick({
      name: entity.tabName,
      index: entity.tabIndex
    });
  }

  getTabs() {
    return this.props.tabs.map((tabEntity, idx) => {
      const key = `${tabEntity}-${idx}`;

      if (idx === this.props.activeTab) {
        return <NavListItemAdjust paddingRight={'0'} key={key}>
          <NavSubLinkActive className='cursor-pointer'
                            active={true}>
            {tabEntity}
          </NavSubLinkActive>
        </NavListItemAdjust>;
      }

      return <NavListItemAdjust paddingRight={'0'} key={key}>
        <NavSubLinkEnabled className='cursor-pointer'
                           onClick={this.handleClick.bind(this, {tabName: tabEntity, tabIndex: idx})}>
          {tabEntity}
        </NavSubLinkEnabled>
      </NavListItemAdjust>;
    });
  }

  render() {
    return <NavMainUl>
      {this.getTabs()}
    </NavMainUl>;
  }
}
