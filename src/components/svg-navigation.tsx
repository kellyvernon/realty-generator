import React from 'react';
import {MenuPolygon, MenuPolygonActive, MenuPolygonDisabled} from './main-menu-item';
import styled from 'styled-components';
import {INavigationResponse} from './i-navigation-response';

const StyledSvg = styled.svg`
  display: block;
  position: relative;
  left: 15px;
`;

export interface ISvgNavigationProps {
  icon?: any;
  tabName?: string,
  tabs: string[],
  areActive: boolean[],
  handleClick: (response:INavigationResponse) => void;
}

export class SvgNavigation extends React.Component<ISvgNavigationProps> {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(tabName: string) {
    this.props.handleClick({
      tabName
    });
  }

  getPolygons() {
    return this.props.tabs.map((tabEntity, idx) => {
      const spacing = 2;
      const getNext = val => idx * (219 + spacing) + val;
      const points = `${getNext(2)},0 ${getNext(222)},0 ${getNext(379)},119 ${getNext(341)},150 ${getNext(121)},150 ${getNext(160)},119`;
      const key = `poly-${idx}`;

      if (this.props.tabName === tabEntity) {
        return <MenuPolygonActive key={key} points={points} onClick={this.handleClick.bind(this, tabEntity)}/>;
      }

      if (idx === 0 && this.props.areActive[idx]) {
        return <MenuPolygon key={key} points={points} onClick={this.handleClick.bind(this, tabEntity)}/>;
      }

      return this.props.areActive[idx - 1]
        ? <MenuPolygon key={key} points={points} onClick={this.handleClick.bind(this, tabEntity)}/>
        : <MenuPolygonDisabled key={key} points={points}/>;
    });
  }

  render() {
    return <StyledSvg height='150' width='825' xmlns='http://www.w3.org/2000/svg'>
      {this.getPolygons()}
    </StyledSvg>;
  }
}
