import React from 'react';
import {Fieldset} from './field-set';
import {Label} from './label';
import styled from 'styled-components';
import {IChangeResult} from './goal-validation-input';
import {IValidationInputState} from './i-validation-input-state';

const CheckFieldset = styled(Fieldset)`
  padding-left:0;
`;

const CheckLabel = styled(Label)`
  -webkit-flex-direction: row;
  flex-direction: row;
  align-items: center;

  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;

  input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
    height: 0;
    width: 0;
  }

  .checkmark {
    position: relative;
    top: 0;
    left: 0;
    height: 35px;
    width: 35px;
    border-radius: .2em;
    padding: 10px 0 10px 10px;
    margin: 0 0 0 10px;
    background-color: ${'#9EFFC8'};
  }

  &:hover input ~ .checkmark {
    background-color: ${'#61ffa5'};
  }

  .checkmark:after {
    content: "";
    position: absolute;
    display: none;
  }

  input:checked ~ .checkmark:after {
    display: block;
  }
  .checkmark:checked ~ .checkmark:after {
    display: block;
  }

  .checkmark:after {
    left: 13px;
    top: 5px;
    width: 10px;
    height: 22px;
    border: solid black;
    border-width: 0 2px 2px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
  }
`;

const getInputName = (name: string): string => {
  const rangeInput = 'checkboxInput';

  return name === undefined || name === null || name === ''
    ? rangeInput
    : `${rangeInput}-${name}`;
}

export interface ICheckboxValidationInputProps {
  value: boolean;
  title: string;
  name?: string;
  onChange: (result: IChangeResult<boolean>) => void;
}

export class CheckboxValidationInput extends React.Component<ICheckboxValidationInputProps, IValidationInputState<boolean>> {
  private readonly input: React.RefObject<HTMLInputElement>;

  static defaultProps = {
    value: false
  }

  constructor(props) {
    super(props);
    this.input = React.createRef<HTMLInputElement>();
    this.state = {
      value: props.value,
      doLoad: true
    };
  }

  componentDidMount() {
    if (this.state.doLoad) {
      this.setState({doLoad: false}, () => {
        this.props.onChange(this.getOnChangeResult(this.input.current ? this.input.current.checked : this.props.value));
      });
    }
  }

  componentWillUnmount() {
    this.setState({doLoad: true});
  }

  getOnChangeResult(value: boolean): IChangeResult<boolean> {
    return {
      value,
      isValid: true
    };
  }

  /**
   *
   * @param {{target:{value:string}}} event
   */
  onChange(event) {
    if (this.props.onChange) {
      this.props.onChange(this.getOnChangeResult(event.target.checked));
    }
  }

  onKeyUp(event) {
    if (event.keyCode !== 9) {
      return;
    }

    if (this.props.onChange) {
      this.props.onChange(this.getOnChangeResult(event.target.checked));
    }
  }

  render() {
    const inputName = getInputName(this.props.name);
    return <CheckFieldset>
      <CheckLabel htmlFor={inputName}>
        {this.props.title}
        <input ref={this.input} type='checkbox' name={inputName} id={inputName}
               checked={this.props.value}
               onChange={this.onChange.bind(this)}
               onLoad={this.onChange.bind(this)}
               onKeyUp={this.onKeyUp.bind(this)}/>
        <span className={'checkmark'}/>
      </CheckLabel>
    </CheckFieldset>;
  }
}
