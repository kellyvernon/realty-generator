import React from 'react';
import {Label} from './label';
import {Input} from './input';
import {Fieldset} from './field-set';
import styled from 'styled-components';
import {IValidationInputState} from './i-validation-input-state';

const SpanPaddingLeft = styled.span`
  flex-basis:auto;
  padding-left: 0.5em;
  color: white;
  font-size: 3.1em;
  font-weight: bold;
`;

const FieldsetUp = styled(Fieldset)`
  padding-top: 0;
  margin-top: -16px;
`;

const LabelSpan = styled.span`
  color: white;
  font-size: 2.3em;
  font-weight: bold;
  -webkit-text-stroke: 2px #254B50;
`;

const LargeInput = styled(Input)`
    color: white;
    text-align:center;
    font-size: 3.1em;
    font-weight: bold;
    width: 200px;
`;

const getRangeName = (name): string => {
  const rangeInput = 'rangeValidationInput';

  return name === undefined || name === null || name === ''
    ? rangeInput
    : `${rangeInput}-${name}`;
}

export class GoalValidationInput extends React.Component<IGoalValidationInputProps, IValidationInputState<number>> {
  private readonly input: React.RefObject<HTMLInputElement>;

  static defaultProps = {
    value: 0,
    min: 0,
    max: 1
  }

  constructor(props) {
    super(props);
    this.input = React.createRef<HTMLInputElement>();
    this.state = {
      value: props.value,
      doLoad: true
    };
  }

  componentDidMount() {
    if (this.state.doLoad) {
      this.setState({doLoad: false}, () => {
        this.props.onChange(this.getOnChangeResult(this.input.current ? Number(this.input.current.value) : this.props.value));
      });
    }
  }

  componentWillUnmount() {
    this.setState({doLoad: true});
  }

  getOnChangeResult(value) {
    const isValid = value ? (value >= this.props.min && value <= this.props.max) : false;
    return {
      value: Number(value),
      isValid
    };
  }

  /**
   *
   * @param {{target:{value:string}}} event
   */
  onChange(event) {
    if (this.props.onChange) {
      this.props.onChange(this.getOnChangeResult(Number(event.target.value)));
    }
  }

  onKeyUp(event) {
    if (event.keyCode !== 9) {
      return;
    }

    if (this.props.onChange) {
      this.props.onChange(this.getOnChangeResult(Number(event.target.value)));
    }
  }

  render() {
    const prefixDom = this.props.prefix ? this.props.prefix : null;
    const suffixDom = this.props.suffix ? this.props.suffix : null;

    const rangeName = getRangeName(this.props.name);
    return <FieldsetUp>
      <Label htmlFor={rangeName}>
        <LabelSpan>
          {this.props.title}:
        </LabelSpan>
        {prefixDom && <SpanPaddingLeft className='prefix'>
          {prefixDom}
        </SpanPaddingLeft>}
        <LargeInput backgroundColor={'transparent'} noSpinners={true} hasUnderline={true}
                    ref={this.input} type='number' name={rangeName} id={rangeName}
                    min={this.props.min} max={this.props.max}
                    placeholder={this.props.min.toString()}
                    value={this.props.value}
                    onChange={this.onChange.bind(this)}
                    onLoad={this.onChange.bind(this)}
                    onKeyUp={this.onKeyUp.bind(this)}/>
        {suffixDom && <SpanPaddingLeft className='suffix'>
          {suffixDom}
        </SpanPaddingLeft>}
      </Label>
    </FieldsetUp>;
  }
}

export interface IChangeResult<T> {
  value: T;
  isValid: boolean
}

export interface IGoalValidationInputProps {
  value?: number;
  suffix?: string;
  prefix?: string;
  title: string;
  name?: string;
  min: number;
  max: number;
  onChange: (result: IChangeResult<number>) => void;
}
