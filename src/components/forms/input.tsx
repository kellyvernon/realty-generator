import styled from 'styled-components';

export interface IInputProps {
  backgroundColor?: string;
  hasUnderline?: boolean;
  noSpinners?: boolean;
}

const transition = 'background-color .4s ease-out';
export const Input = styled.input.attrs<IInputProps>(p => {
  if (!!p.hasUnderline) {
    p.hasUnderline = false;
  }
  if (!!p.noSpinners) {
    p.noSpinners = true;
  }
})<IInputProps>`
  -webkit-transition: ${transition};
  -moz-transition: ${transition};
  -o-transition: ${transition};
  transition: ${transition};
  background-color: ${props => props.backgroundColor || '#9EFFC8'};
  outline: 0;

  ${props => props.hasUnderline
  ? `
     border-color: white;
     border-width: 0px 0px 5px 0;
  `
  : `
    border: 0;
    border-radius: .2em;
  `
}

  &:focus {
    background-color: ${props => props.backgroundColor || '#baffd8'};
  }

  &:disabled {
    background-color: ${props => props.backgroundColor || '#698679'};
  }


  padding: 10px 0 10px 10px;
  margin: 0 0 0 10px;
  position: relative;
  flex-grow: 1;

  ${props => props.noSpinners
  ? `&[type=number]::-webkit-inner-spin-button,
     &[type=number]::-webkit-outer-spin-button {
      -webkit-appearance: none;
      margin: 0;
    }`

  : `&[type="number"]::-webkit-outer-spin-button,
     &[type="number"]::-webkit-inner-spin-button {
       opacity: .5; /* shows Spin Buttons per default (Chrome >= 39) */
       position: absolute;
       top: 0;
       right: 0;
       bottom: 0;
     }

     &[type="number"]::-webkit-inner-spin-button:hover,
     &[type="number"]::-webkit-inner-spin-button:active{
       box-shadow: 0 0 2px #0CF;
       opacity: .8;
     }`
}

  &:-webkit-autofill {
    background: black;
    color: red;
  }
  -webkit-appearance: none;
  -moz-appearance: none;
`;
