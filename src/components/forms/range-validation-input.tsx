import React, {createRef} from 'react';
import {Label} from './label';
import {Input} from './input';
import {Fieldset} from './field-set';
import styled from 'styled-components';
import {IValidationInputState} from './i-validation-input-state';

const SpanPaddingLeft = styled.span`
  flex-basis:auto;
  padding-left: 0.5em;
`;

const getRangeName = (name): string => {
  const rangeInput = 'rangeValidationInput';

  return name === undefined || name === null || name === ''
    ? rangeInput
    : `${rangeInput}-${name}`;
}

export interface IRangeValidationResult {
  value: number;
  isValid: boolean;
}

export interface IRangeValidationInputProps {
  title: string;
  min: number;
  max: number;
  onChange: (evt: IRangeValidationResult) => void;
  value?: number;
  suffix?: string;
  prefix?: string;
  name?: string;
  isEnabled?: boolean;
}

export class RangeValidationInput extends React.Component<IRangeValidationInputProps, IValidationInputState<number>> {
  private readonly input: React.RefObject<HTMLInputElement>;

  static defaultProps = {
    value: 0,
    min: 0,
    max: 1,
    isEnabled: true
  }

  constructor(props) {
    super(props);
    this.input = createRef<HTMLInputElement>();
    this.state = {
      value: props.value,
      doLoad: true
    };
  }

  componentDidMount() {
    if (this.state.doLoad) {
      this.setState({doLoad: false}, () => {
        this.props.onChange(this.getOnChangeResult(this.input.current ? Number(this.input.current.value) : this.props.value));
      });
    }
  }

  componentWillUnmount() {
    this.setState({doLoad: true});
  }

  getOnChangeResult(value): IRangeValidationResult {
    const isValid = value ? (value >= this.props.min && value <= this.props.max) : false;
    return {
      value: Number(value),
      isValid
    };
  }

  /**
   *
   * @param {{target:{value:string}}} event
   */
  onChange(event) {
    if (this.props.onChange) {
      this.props.onChange(this.getOnChangeResult(Number(event.target.value)));
    }
  }

  onKeyUp(event) {
    if (event.keyCode !== 9) {
      return;
    }

    if (this.props.onChange) {
      this.props.onChange(this.getOnChangeResult(Number(event.target.value)));
    }
  }

  render() {
    const prefixDom = this.props.prefix ? this.props.prefix : null;
    const suffixDom = this.props.suffix ? this.props.suffix : null;

    const rangeName = getRangeName(this.props.name);
    return <Fieldset>
      <Label htmlFor={rangeName}>
        <span className='title'>
          {this.props.title}:
        </span>
        {prefixDom && <SpanPaddingLeft className='prefix'>
          {prefixDom}
        </SpanPaddingLeft>}
        <Input ref={this.input}
               type='number'
               name={rangeName}
               id={rangeName}
               min={this.props.min} max={this.props.max}
               placeholder={this.props.min.toString()}
               value={this.props.value.toString()}
               disabled={!!this.props.isEnabled ? !this.props.isEnabled : false}
               onChange={this.onChange.bind(this)}
               onLoad={this.onChange.bind(this)}
               onKeyUp={this.onKeyUp.bind(this)}
               title={`Range between ${this.props.min} and ${this.props.max}`}
        />
        {suffixDom && <SpanPaddingLeft className='suffix'>
          {suffixDom}
        </SpanPaddingLeft>}
      </Label>
    </Fieldset>;
  }
}
