import styled from 'styled-components';

export const Fieldset = styled.fieldset`
  display: -webkit-flex;
  display: flex;
  -webkit-flex-direction: column;
  flex-direction: column;
  align-items: center;
  border: none;
`;
