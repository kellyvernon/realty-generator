import styled from 'styled-components';

export const Label = styled.label`
  display: -webkit-flex;
  display: flex;

  justify-content: start;

  align-content: space-evenly;
  align-items: baseline;
`;
