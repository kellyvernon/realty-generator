import {IInputState} from './i-input-state';

export interface IValidationInputState<T> extends IInputState{
    value: T;
}

