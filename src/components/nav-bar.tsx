import React from 'react';
import styled from 'styled-components';
import {SvgNavigation} from './svg-navigation';
import {DivFlexContainer} from './div-flex-container';
import {INavigationResponse} from './i-navigation-response';

const START_Y = -300;

const LogoImg = styled.img`
  position: relative;
  left: 48px;
  top: -22px;
  content:url('images/rental-generator-2.png');
`;

const DivWrapper = styled(DivFlexContainer)`
    align-items: baseline;
`;

export class NavigationBar extends React.Component<INavigationBarProps> {
  static defaultProps = {
    icon: undefined,
    tabs: [],
    tabName: null,
    areActive: []
  }

  constructor(props) {
    super(props);
    this.state = {
      activeTab: 0,
      startY: START_Y
    };
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(tabObject: INavigationResponse) {
    this.props.handleClick({
      tabName: tabObject.tabName
    });
  }

  render() {
    return <DivWrapper>
      <LogoImg/>
      <SvgNavigation handleClick={this.handleClick.bind(this)}
                     tabName={this.props.tabName}
                     areActive={this.props.areActive}
                     icon={this.props.icon}
                     tabs={this.props.tabs}/>
    </DivWrapper>;
  }
}

export interface INavigationBarProps {
  icon: any,
  tabName?: string,
  tabs: string[],
  areActive: boolean[],
  handleClick: (response: INavigationResponse) => void;
}
