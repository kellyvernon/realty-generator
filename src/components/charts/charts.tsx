import React from 'react';
import {CurrencyOverTime} from './currency-over-time';
import {RentalsOverTime} from './rentals-over-time';
import styled from 'styled-components';
import {RentalCollection, LedgerItemType, Rental} from 'realty-generator-core';
import {ILedgerItemBalance} from 'realty-generator-core/dist/src/ledger/ledger';

const Div = styled.div`
  padding: 100px;
`;

export const Charts: React.FunctionComponent<IChartsProps> = (props) => {
  const getUniqueDates = props.ledger
    .map(l => l.item.created)
    .filter((e, i, self) => self.findIndex(d => d.getTime() === e.getTime()) === i);

  const cashFlowOverTime = [];
  getUniqueDates.forEach(d => {
    const cashFlow = props.ledger.filter(e => e.item.created.getTime() === d.getTime() && e.item.type === LedgerItemType.CashFlow);
    const totalCashFlow = cashFlow.reduce((total, entity) => {
      total += entity.item.amount;
      return total;
    }, 0);
    cashFlowOverTime.push({x0: d, x: d.getTime(), y: totalCashFlow});
  });

  const rentalsOverTime = [];

  getUniqueDates.forEach(d => {
    if (!props.rentalCollection) {
      rentalsOverTime.push({x0: d, x: d.getTime(), y: 0});
    } else {
      rentalsOverTime.push({x0: d, x: d.getTime(), y: props.rentalCollection.activeRentals(d).length});
    }
  });

  return <div>
    <Div>
      <h5>Cash flow Over Time</h5>
      <CurrencyOverTime data={cashFlowOverTime}/>
    </Div>
    <Div>
      <h5>Rentals Over Time</h5>
      <RentalsOverTime data={rentalsOverTime}/>
    </Div>
  </div>;
}

export interface IChartsProps {
  ledger: ILedgerItemBalance[],
  rentalCollection: RentalCollection<Rental>
}

