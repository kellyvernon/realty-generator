import React from 'react';
import {FlexibleXYPlot, HorizontalGridLines, VerticalBarSeries, VerticalGridLines, XAxis, YAxis} from 'react-vis';
import {IDataPoint} from './i-data-point';

import moment from 'moment';

export interface IOverTimeProps {
  data: IDataPoint[];
}

export const CurrencyOverTime: React.FunctionComponent<IOverTimeProps> = ({data}) => {
  const yDomain = data.reduce((res, row) => ({
      max: Math.max(res.max, row.y),
      min: Math.min(res.min, row.y)
    }),
    {max: -Infinity, min: Infinity}
  );

  return <FlexibleXYPlot height={300}
                         xType='time'
                         style={{overflow: 'visible'}}
                         yDomain={[yDomain.min, yDomain.max]}>
    <XAxis tickFormat={v => moment(v).format('MMM, YYYY')} tickLabelAngle={-90}/>
    <YAxis title='Cash flow'/>
    <HorizontalGridLines/>
    <VerticalGridLines/>
    <VerticalBarSeries data={data} animation/>
  </FlexibleXYPlot>;
}
