import React from 'react';
import {FlexibleXYPlot, HorizontalGridLines, VerticalBarSeries, VerticalGridLines, XAxis, YAxis} from 'react-vis';
import {IOverTimeProps} from './currency-over-time';

import moment from 'moment';

export const RentalsOverTime: React.FunctionComponent<IOverTimeProps> = ({data}) => {
  const yDomain = data.reduce(
    (res, row) => {
      return {
        max: Math.max(res.max, row.y),
        min: Math.min(res.min, row.y)
      };
    },
    {max: -Infinity, min: Infinity}
  );

  return <FlexibleXYPlot height={300}
                         xType='time'
                         style={{overflow: 'visible'}}
                         yDomain={[yDomain.min, yDomain.max]}>
    <XAxis tickFormat={v => moment(v).format('MMM, YYYY')} tickLabelAngle={-90}/>
    <YAxis title={'Rentals'}/>
    <HorizontalGridLines/>
    <VerticalGridLines/>
    <VerticalBarSeries data={data} animation/>
  </FlexibleXYPlot>;
}
