export interface IDataPoint {
    x: number;
    y: number;
}
