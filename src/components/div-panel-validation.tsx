import React from 'react';
import {ChildrenContainer, DivPanelStyle} from './div-panel-style';
import styled from 'styled-components';

export enum DivPanelValidationTypes {
  Invalid = 0,
  Valid = 1,
  Optional = 2
}

const DivIsValidGood = '#00ff00';
const DivIsValidBad = '#ff0000';

export interface IDivIsValidProps {
  isValid: DivPanelValidationTypes;
}

const transition = 'background-color .4s ease-out';
const DivIsValid = styled.div<IDivIsValidProps>`
  -webkit-transition: ${transition};
  -moz-transition: ${transition};
  -o-transition: ${transition};
  transition: ${transition};

  width: 10px;
  flex-direction: column;

  background: ${props => {
  if (props.isValid === DivPanelValidationTypes.Valid) {
    return DivIsValidGood;
  }

  if (props.isValid === DivPanelValidationTypes.Invalid) {
    return DivIsValidBad;
  }

  return 'grey';
}}`;

export interface IDivPanelValidationProps {
  height?: number;
  backgroundColor?: string;
  allDivPanelValidationTypes: DivPanelValidationTypes[];
  divPanelValidationType: DivPanelValidationTypes;
  children: React.ReactNode[] | React.ReactNode;
}

export class DivPanelValidation extends React.Component<IDivPanelValidationProps> {
  static defaultProps = {
    title: '',
    height: 0,
    backgroundColor: '#254B50',
    divPanelValidationType: DivPanelValidationTypes.Optional,
    allDivPanelValidationTypes: [],
    children: []
  }

  evalResults(containerTypes: DivPanelValidationTypes[]): DivPanelValidationTypes {
    if (containerTypes.every(s => s === undefined)) {
      return this.props.divPanelValidationType;
    }

    if (containerTypes.some(s => s === DivPanelValidationTypes.Invalid)) {
      return DivPanelValidationTypes.Invalid;
    }

    if (containerTypes.some(s => s === DivPanelValidationTypes.Valid)) {
      return DivPanelValidationTypes.Valid;
    }

    return DivPanelValidationTypes.Optional;
  }

  render() {
    return <DivPanelStyle height={this.props.height} backgroundColor={this.props.backgroundColor}>
      <ChildrenContainer>
        {this.props.children}
      </ChildrenContainer>
      <DivIsValid isValid={this.evalResults(this.props.allDivPanelValidationTypes)}/>
    </DivPanelStyle>;
  }
}
