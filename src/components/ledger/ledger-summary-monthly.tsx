import React from 'react';
import {
  DivFlexContainerAlignItems,
  DivFlexContainerJustifyContent
} from '../div-flex-container';
import {Col, Row} from '../row';
import {currencyFormatter} from 'realty-generator-core';
import styled from 'styled-components';
import {Button} from '../button';
import {DivPanel} from '../div-panel';
import {DivInputContainer} from '../div-Input-container';
import {LedgerSummaryTransaction} from './ledger-summary-transaction';
import {ILedgerSummaryProps} from './i-ledger-summary-props';
import {ILedgerSummaryState} from './i-ledger-summary-state';
import moment from 'moment';

const ButtonExpose = styled(Button)`
  width: 30px;
`;

const Title = styled.h3`
  position: relative;
  top: 1.3em;
  color: white;
  margin: -15px 0 21px 0;
`;

export class LedgerSummaryMonthly extends React.Component<ILedgerSummaryProps, ILedgerSummaryState> {
  static defaultProps = {
    data: {
      date: null,
      balance: 0,
      cashFlow: 0,
      averageCashFlow: 0,
      equity: 0,
      purchases: 0,
    },
    ledger: null
  }

  constructor(props) {
    super(props);

    this.state = {
      buttonName: '+'
    };
  }

  /**
   *
   * @param evt {{data: any}}
   */
  handleClick(evt) {
    this.setState({
      buttonName: this.state.buttonName === '+' ? '-' : '+'
    });
  }

  render() {
    const headerKey = `${Date.now()}-${0}-header}`;
    return <DivInputContainer width={'100%'}>
      <Title>{moment(this.props.data.date).format('MMMM')}</Title>
      <DivPanel backgroundColor={'#1d1d26'} padding={'10px 0 0 0'}>
        <Row>
          <Col flexBasis={'50px'} alignItems={DivFlexContainerAlignItems.CENTER}
               justifyContent={DivFlexContainerJustifyContent.CENTER}>
            <ButtonExpose className={'mx-auto'}
                          onClick={this.handleClick.bind(this, this.props.data)}>{this.state.buttonName}</ButtonExpose>
          </Col>
          <Col flexGrow={6}>
            <Row key={`this-is-the-header-row-0-${Date.now()}`}>
              <Col flexGrow={1} key={headerKey + '-column-cashflow'}><strong>CashFlow</strong></Col>
              <Col flexGrow={1} key={headerKey + '-column-avg-cashflow'}><strong>Avg CashFlow</strong></Col>
              <Col flexGrow={1} key={headerKey + '-column-purchases'}><strong>Purchases</strong></Col>
              <Col flexGrow={1} key={headerKey + '-column-equity'}><strong>Equity</strong></Col>
              <Col flexGrow={1} key={headerKey + '-column-balance'}><strong>Balance</strong></Col>
            </Row>
            <Row>
              <Col flexGrow={1} className='text-right'>{currencyFormatter(this.props.data.cashFlow)}</Col>
              <Col flexGrow={1} className='text-right'>{currencyFormatter(this.props.data.averageCashFlow)}</Col>
              <Col flexGrow={1} className='text-right'>{currencyFormatter(this.props.data.purchases)}</Col>
              <Col flexGrow={1} className='text-right'>{currencyFormatter(this.props.data.equity)}</Col>
              <Col flexGrow={1} className='text-right'>{currencyFormatter(this.props.data.balance)}</Col>
            </Row>
          </Col>
        </Row>
        {this.state.buttonName === '-' &&
          <LedgerSummaryTransaction data={this.props.ledger.getLedgerItemBalances(this.props.data.date)}/>
        }
      </DivPanel>
    </DivInputContainer>;
  }
}

/*LedgerSummaryMonthly.propTypes = {
  data: PropTypes.shape({
    date: PropTypes.instanceOf(Date),
    balance: PropTypes.number,
    cashFlow: PropTypes.number,
    averageCashFlow: PropTypes.number,
    equity: PropTypes.number,
    purchases: PropTypes.number,
  }),
  ledger: PropTypes.arrayOf(PropTypes.shape({
    item: PropTypes.shape({
      amount: PropTypes.number,
      created: PropTypes.instanceOf(Date),
      note: PropTypes.string,
      type: PropTypes.string,
      referenceId: PropTypes.string,
      total: PropTypes.func
    }),
    balance: PropTypes.number,
  }))
}*/
