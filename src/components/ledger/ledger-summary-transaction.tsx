import React from 'react';
import {Col, Row} from '../row';
import {currencyFormatter} from 'realty-generator-core';
import {DivInputContainer} from '../div-Input-container';
import styled from 'styled-components';
import {ILedgeTransactionsProps} from './i-ledge-transactions-props';

import moment from 'moment';

const RowTransAction = styled(Row)`
  border: 0 solid #1d1d26;border-bottom-width: 1px;

  padding-top: 10px;
  padding-bottom: 10px;
`;

export class LedgerSummaryTransaction extends React.Component<ILedgeTransactionsProps> {
  static defaultProps = {
    data: []
  }

  constructor(props) {
    super(props);
  }

  render() {
    const ledgerItems = this.props.data.map((li, idx) => {
      const key = `${li.item.created.getTime()}-${idx}-${Date.now()}-record`;
      return <RowTransAction key={`${key}-row`}>
        <Col flexGrow={1} key={key + '-column-created'}>{moment(li.item.created).format('MMM, YYYY')}</Col>
        <Col flexGrow={1} key={key + '-column-type'}>{li.item.type}</Col>
        <Col flexGrow={1} key={key + '-column-referenceId'}>{li.item.referenceId}</Col>
        <Col flexGrow={1} className='text-right' key={key + '-column-amount'}>{currencyFormatter(li.item.amount)}</Col>
        <Col flexGrow={1} className='text-right' key={key + '-column-balance'}>{currencyFormatter(li.balance)}</Col>
      </RowTransAction>;
    });

    const headerKey = `${Date.now()}-${0}-header}`;
    return <DivInputContainer className={'LedgerSummaryTransaction'} width={'100%'} backgroundColor={'#254B50'}>
      <RowTransAction key={`this-is-the-header-row-0-${Date.now()}`}>
        <Col flexGrow={1} key={headerKey + '-column-created'}><strong>Created</strong></Col>
        <Col flexGrow={1} key={headerKey + '-column-type'}><strong>Type</strong></Col>
        <Col flexGrow={1} key={headerKey + '-column-referenceId'}><strong>Reference Id</strong></Col>
        <Col flexGrow={1} key={headerKey + '-column-amount'}><strong>Amount</strong></Col>
        <Col flexGrow={1} key={headerKey + '-column-balance'}><strong>Balance</strong></Col>
      </RowTransAction>
      {ledgerItems}
    </DivInputContainer>;
  }
}
