import {ILedger, ILedgerSummary} from 'realty-generator-core';

export interface ILedgerSummaryAnnualProps extends ILedgerSummaryProps {
  goal: number;
}

export interface ILedgerSummaryProps {
  ledger: ILedger;
  data: ILedgerSummary;
}

