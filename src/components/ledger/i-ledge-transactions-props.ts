import {ILedgerItemBalance} from 'realty-generator-core';

export interface ILedgeTransactionsProps {
    data: ILedgerItemBalance[];
}
