import React from 'react';
import {LedgerSummaryAnnual} from './ledger-summary-annual';
import styled from 'styled-components';
import {
  DivFlexContainer,
  DivFlexContainerAlignItems,
  DivFlexContainerDirections
} from '../div-flex-container';
import {ILedger} from 'realty-generator-core';

const DivFlexContainerColumn = styled(DivFlexContainer).attrs(() => ({
  alignItems: DivFlexContainerAlignItems.CENTER,
  direction: DivFlexContainerDirections.COLUMN
}))`
  flex-flow: column;
  padding-left:40px;
`;

export interface ILedgerMainProps {
  ledger: ILedger;
  goal: number;
}

export class LedgerMain extends React.Component<ILedgerMainProps> {
  static defaultProps = {
    ledger: null,
    goal: 0
  };

  constructor(props) {
    super(props);
  }

  render() {
    const annualBalances = this.props.ledger.getAnnualSummaries().map((summary, idx) => {
      const key = `${summary.date.getTime()}-${idx}-${Date.now()}-record`;
      return <LedgerSummaryAnnual key={`${key}-row`}
                                  data={summary}
                                  ledger={this.props.ledger}
                                  goal={this.props.goal}/>;
    });

    return <DivFlexContainerColumn>
      {annualBalances}
    </DivFlexContainerColumn>;
  }
}

