import React from 'react';
import {DivFlexContainerAlignItems, DivFlexContainerJustifyContent} from '../div-flex-container';
import {Col, Row} from '../row';
import {currencyFormatter} from 'realty-generator-core';
import styled from 'styled-components';
import {Button} from '../button';
import {LedgerSummaryMonthly} from './ledger-summary-monthly';
import {InputContainer} from '../input-container';
import {DivPanelValidationTypes} from '../div-panel-validation';
import {ILedgerSummaryAnnualProps} from './i-ledger-summary-props';
import {ILedgerSummaryState} from './i-ledger-summary-state';

const ButtonExpose = styled(Button)`
  width: 30px;
`;

export class LedgerSummaryAnnual extends React.Component<ILedgerSummaryAnnualProps, ILedgerSummaryState> {
  static defaultProps = {
    data: {
      date: null,
      balance: 0,
      cashFlow: 0,
      averageCashFlow: 0,
      equity: 0,
      purchases: 0,
    },
    ledger: null,
    goal: 0
  }

  constructor(props) {
    super(props);

    this.state = {
      buttonName: '+'
    };
  }

  handleClick(evt) {
    this.setState({
      buttonName: this.state.buttonName === '+' ? '-' : '+'
    });
  }

  getMonthly() {
    if (this.state.buttonName === '-') {
      const monthlySummaries = this.props.ledger.getMonthlySummaries(this.props.data.date.getFullYear());
      return monthlySummaries.map((summary, index) => {
          const headerKey = `${Date.now()}-${index}-LedgerSummaryMonthly-header}`;
          return <Row key={headerKey}>
            <LedgerSummaryMonthly key={headerKey}
                                  data={summary}
                                  ledger={this.props.ledger}/>
          </Row>;
        }
      );
    }

    return [];
  }


  render() {
    const headerKey = `${Date.now()}-LedgerSummaryAnnual-header-${this.props.data.date.getFullYear()}`;
    const monthly = this.getMonthly();

    return <InputContainer title={`Year ${this.props.data.date.getFullYear()}`}
                           inputContainerType={DivPanelValidationTypes.Optional}
                           allInputContainerTypes={[this.props.data.averageCashFlow >= this.props.goal ? DivPanelValidationTypes.Valid : DivPanelValidationTypes.Optional]}>
      <Row>
        <Col flexBasis={'50px'} alignItems={DivFlexContainerAlignItems.CENTER}
             justifyContent={DivFlexContainerJustifyContent.CENTER}>
          <ButtonExpose className={'mx-auto'}
                        onClick={this.handleClick.bind(this, this.props.data)}>
            {this.state.buttonName}
          </ButtonExpose>
        </Col>
        <Col flexGrow={6}>
          <Row key={`this-is-the-header-row-0-${Date.now()}`}>
            <Col flexGrow={1} key={headerKey + '-column-cashflow'}><strong>CashFlow</strong></Col>
            <Col flexGrow={1} key={headerKey + '-column-avg-cashflow'}><strong>Mo CashFlow</strong></Col>
            <Col flexGrow={1} key={headerKey + '-column-purchases'}><strong>Purchases</strong></Col>
            <Col flexGrow={1} key={headerKey + '-column-equity'}><strong>Equity</strong></Col>
            <Col flexGrow={1} key={headerKey + '-column-balance'}><strong>Balance</strong></Col>
          </Row>
          <Row>
            <Col flexGrow={1} className='text-right'>{currencyFormatter(this.props.data.cashFlow)}</Col>
            <Col flexGrow={1} className='text-right'>{currencyFormatter(this.props.data.averageCashFlow)}</Col>
            <Col flexGrow={1} className='text-right'>{currencyFormatter(this.props.data.purchases)}</Col>
            <Col flexGrow={1} className='text-right'>{currencyFormatter(this.props.data.equity)}</Col>
            <Col flexGrow={1} className='text-right'>{currencyFormatter(this.props.data.balance)}</Col>
          </Row>
        </Col>
      </Row>
      {monthly}
    </InputContainer>;
  }
}
