import styled from 'styled-components';
import {LinkColorConstants} from '../constants/link-color-constants';

export interface ISpanIcon {
  borderColor?: string;
}

export const SpanIcon = styled.span<ISpanIcon>`
  display: flex;
  line-height: 1px;
  width: 6em;
  height: 6em;
  border-width: 0.8em;
  border-style: solid;
  border-color: ${props => props.borderColor};
  border-radius: 50%;
  -moz-border-radius: 50%;
  -webkit-border-radius: 50%;

  &:hover {
    cursor: pointer;
    border-color: ${LinkColorConstants.highlight};
    box-shadow: 0 0 15px ${LinkColorConstants.highlightBlue};
  }

  &:disabled {
     border-color: ${LinkColorConstants.disabled};
  }
  &:after {
    display: flex;
  }
`;
