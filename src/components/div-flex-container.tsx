import styled from 'styled-components';

export enum DivFlexContainerAlignItems {
  FLEX_START = 'flex-start',
  FLEX_END = 'flex-end',
  CENTER = 'center',
  BASELINE = 'baseline',
  BASELINE_FIRST = `first baseline`,
  BASELINE_LAST = `last baseline`,
  STRETCH = 'stretch'
}

export enum DivFlexContainerDirections {
  COLUMN = 'column',
  COLUMN_REVERSE = 'column-reverse',
  ROW = 'row',
  ROW_REVERSE = 'row-reverse'
}

export enum DivFlexContainerJustifyContent {
  FLEX_START = 'flex-start',
  FLEX_END = 'flex-end',
  CENTER = 'center',
  SPACE_BETWEEN = 'space-between',
  SPACE_AROUND = 'space-around',
  SPACE_EVENLY = 'space-evenly'
}

export interface IDivFlexContainerProps {
  direction?: DivFlexContainerDirections;
  alignItems?: DivFlexContainerAlignItems;
  justifyContent?: DivFlexContainerJustifyContent;
}

export const DivFlexContainer = styled.div<IDivFlexContainerProps>`
  display: -webkit-flex;
  display: flex;

  -webkit-flex-direction: ${props => props.direction || DivFlexContainerDirections.ROW};
  flex-direction: ${props => props.direction || DivFlexContainerDirections.ROW};

  align-items: ${props => props.alignItems || DivFlexContainerAlignItems.STRETCH};

  -webkit-justify-content: ${props => props.justifyContent || DivFlexContainerJustifyContent.FLEX_START};
  justify-content: ${props => props.justifyContent || DivFlexContainerJustifyContent.FLEX_START};
`;
