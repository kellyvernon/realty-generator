import styled from 'styled-components';
import {IDivFlexContainerProps} from './div-flex-container';

export interface IDivPanelStyleProps extends IDivFlexContainerProps {
  backgroundColor: string;
  height?: number;
}

export const DivPanelStyle = styled.div<IDivPanelStyleProps>`
  display: flex;
  flex-direction: row;
  align-items: stretch;
  background: ${props => props.backgroundColor};

  ${props => props.height > 0 ? `height:${props.height}px;` : ''}
`;

export interface IChildrenContainerProps {
  padding?: string;
}

export const ChildrenContainer = styled.div<IChildrenContainerProps>`
  padding: ${props => props.padding ? props.padding : '20px'};
  width: 100%;
`;
