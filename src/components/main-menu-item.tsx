import styled from 'styled-components';

const color = '#007ae6';
const colorDisabled = '#a0a0a0';
const colorHover = '#2693ff';

const transition = 'fill .3s cubic-bezier(0, 1.11, 1, 1);';

export const MenuPolygon = styled.polygon`
  -webkit-transition: ${transition};
  -moz-transition: ${transition};
  -o-transition: ${transition};
  transition: ${transition};

  fill: ${color};
  cursor: initial;

  &:hover{
    fill: ${colorHover};
    cursor: pointer;
  }
`;

export const MenuPolygonDisabled = styled.polygon`
  fill: ${colorDisabled};
  cursor: initial;
`;

export const MenuPolygonActive = styled.polygon`
  fill: ${colorHover};
  cursor: initial;
`;

// may need to go to svg instead
// https://blog.lftechnology.com/using-svg-icons-components-in-react-44fbe8e5f91
// https://css-tricks.com/the-many-ways-to-link-up-shapes-and-images-with-html-and-css/

