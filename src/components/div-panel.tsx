import React from 'react';
import {ChildrenContainer, DivPanelStyle} from './div-panel-style';

export class DivPanel extends React.Component<IDivPanelProps> {
  static defaultProps = {
    title: '',
    height: 0,
    padding: null,
    backgroundColor: '#254B50',
    children: []
  }

  render() {
    return <DivPanelStyle height={this.props.height} backgroundColor={this.props.backgroundColor}>
      <ChildrenContainer padding={this.props.padding}>
        {this.props.children}
      </ChildrenContainer>
    </DivPanelStyle>;
  }
}

export interface IDivPanelProps {
  height?: number;
  padding?: string;
  backgroundColor: string;
  children: React.ReactNode[];
}

