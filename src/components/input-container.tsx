import React from 'react';
import styled from 'styled-components';
import {DivPanelValidation, DivPanelValidationTypes} from './div-panel-validation';
import {DivInputContainer} from './div-Input-container';

const Title = styled.h3`
  position: relative;
  top: 1.2em;
  color: white;
`;

export class InputContainer extends React.Component<IInputContainerProps> {
  static defaultProps = {
    title: '',
    height: 0,
    backgroundColor: '#254B50',
    inputContainerType: DivPanelValidationTypes.Optional,
    allInputContainerTypes: [],
    children: []
  }

  constructor(props) {
    super(props);
  }

  render() {
    return <DivInputContainer>
      <Title>{this.props.title}</Title>
      <DivPanelValidation height={this.props.height}
                          backgroundColor={this.props.backgroundColor}
                          divPanelValidationType={this.props.inputContainerType}
                          allDivPanelValidationTypes={this.props.allInputContainerTypes}>
        {this.props.children}
      </DivPanelValidation>
    </DivInputContainer>;
  }
}

export interface IInputContainerProps {
  title: string;
  height?: number;
  inputContainerType: DivPanelValidationTypes,
  allInputContainerTypes: DivPanelValidationTypes[],
  backgroundColor?: string,
  children: React.ReactNode[] | React.ReactNode;
}
