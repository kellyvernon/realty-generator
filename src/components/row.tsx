import styled from 'styled-components';
import {
  DivFlexContainerDirections,
  DivFlexContainer, DivFlexContainerAlignItems, DivFlexContainerJustifyContent, IDivFlexContainerProps
} from './div-flex-container';

export interface IRowProps extends IDivFlexContainerProps {
  flexGrow?: number;
}

export const Row = styled(DivFlexContainer).attrs(() => ({
  direction: DivFlexContainerDirections.ROW
}))<IRowProps>`
  width: 100%;

  padding: 0.3em;

  flex: ${props => props.flexGrow || 0};
  -webkit-flex: ${props => props.flexGrow || 0};
  -ms-flex: 1: ${props => props.flexGrow || 0};
`;

export interface IColProps extends IDivFlexContainerProps {
  flexGrow?: number;
  flexBasis?: string;
}

export const Col = styled(DivFlexContainer).attrs<IDivFlexContainerProps>(p => ({
  direction: DivFlexContainerDirections.COLUMN,
  alignItems: p.alignItems || DivFlexContainerAlignItems.FLEX_START,
  justifyContent: p.justifyContent || DivFlexContainerJustifyContent.FLEX_START,
}))<IColProps>`
  flex: ${props => props.flexGrow || 0};
  -webkit-flex: ${props => props.flexGrow || 0};
  -ms-flex: ${props => props.flexGrow || 0};
  ${props => props.flexBasis ? `flex-basis: ${props.flexBasis};` : ''}

  padding-left: 15px;
  padding-right: 15px;
`;
