import React from 'react';
import styled from 'styled-components';
import {IRangeValidationResult, RangeValidationInput} from './components/forms/range-validation-input';
import {DivFlexContainer} from './components/div-flex-container';
import {InputContainer} from './components/input-container';
import {PageTitle} from './components/page-title';
import {GoalValidationInput} from './components/forms/goal-validation-input';
import {DivPanelValidationTypes} from './components/div-panel-validation';
import {IInputState} from './components/forms/i-input-state';
import {IUserInputEntity} from './i-user-input-entity';
import {userInputDefault} from './defaults/user-input-default';

const DivFlexContainerColumn = styled(DivFlexContainer)`
  position: relative;
  top: -736px;
  flex-flow: column;
  align-items: center;
`;

const PageTitleNegHeight = styled.div`
`;

const DivWrap = styled.div`
  display: inline-block;
`;

const DivBackgroundImage = styled.div`
    background-image: url('images/goal-background.png');
    background-position-y: 75px;
    background-position-x: 183px;
    overflow: visible;
    width: 1426px;
    height: 736px;
`;

export interface IUserInputState extends IInputState {
  monthlyIncomeAmountGoalIsValid: DivPanelValidationTypes;
  monthlySavedAmountIsValid: DivPanelValidationTypes;
  currentlySavedAmountIsValid: DivPanelValidationTypes;
  minMonthlyCashOnCashAmountHouseIsValid: DivPanelValidationTypes;
  minMonthlyCashOnCashAmountApartmentIsValid: DivPanelValidationTypes;
  minEquityCapturePercentIsValid: DivPanelValidationTypes;
  minInvestAmountIsValid: DivPanelValidationTypes;
  maxInvestAmountIsValid: DivPanelValidationTypes;
}

export interface IUserInputProps {
  id?: string;
  handleChange: (entity: IUserInputEntity) => void;
  userInput: IUserInputEntity;
}

export class UserInput extends React.Component<IUserInputProps, IUserInputState> {
  static defaultProps = {
    userInput: userInputDefault
  }

  constructor(props) {
    super(props);
    this.monthlyIncomeAmountGoalHandleChange = this.monthlyIncomeAmountGoalHandleChange.bind(this);
    this.monthlySavedAmountHandleChange = this.monthlySavedAmountHandleChange.bind(this);
    this.minMonthlyCashOnCashAmountHouseHandleChange = this.minMonthlyCashOnCashAmountHouseHandleChange.bind(this);
    this.minMonthlyCashOnCashAmountApartmentHandleChange = this.minMonthlyCashOnCashAmountApartmentHandleChange.bind(this);
    this.minEquityCapturePercentHandleChange = this.minEquityCapturePercentHandleChange.bind(this);
    this.minInvestAmountHandleChange = this.minInvestAmountHandleChange.bind(this);
    this.maxInvestAmountHandleChange = this.maxInvestAmountHandleChange.bind(this);
    this.currentlySavedAmountHandleChange = this.currentlySavedAmountHandleChange.bind(this);

    this.state = {
      doLoad: true,
      monthlyIncomeAmountGoalIsValid: DivPanelValidationTypes.Invalid,
      monthlySavedAmountIsValid: DivPanelValidationTypes.Invalid,
      currentlySavedAmountIsValid: DivPanelValidationTypes.Invalid,
      minMonthlyCashOnCashAmountHouseIsValid: DivPanelValidationTypes.Invalid,
      minMonthlyCashOnCashAmountApartmentIsValid: DivPanelValidationTypes.Invalid,
      minEquityCapturePercentIsValid: DivPanelValidationTypes.Invalid,
      minInvestAmountIsValid: DivPanelValidationTypes.Invalid,
      maxInvestAmountIsValid: DivPanelValidationTypes.Invalid
    };
  }

  componentDidMount() {
    if (this.state.doLoad) {
      this.setState({doLoad: false}, () => {
        this.handleChange(this.getProps());
      });
    }
  }

  componentWillUnmount() {
    this.setState({doLoad: true});
  }

  getProps(): IUserInputEntity {
    return {...this.props.userInput};
  }

  handleChange(result) {
    const isValid =
      this.state.monthlyIncomeAmountGoalIsValid === DivPanelValidationTypes.Valid &&
      this.state.monthlySavedAmountIsValid === DivPanelValidationTypes.Valid &&
      this.state.currentlySavedAmountIsValid === DivPanelValidationTypes.Valid &&
      this.state.minMonthlyCashOnCashAmountHouseIsValid === DivPanelValidationTypes.Valid &&
      this.state.minEquityCapturePercentIsValid === DivPanelValidationTypes.Valid &&
      this.state.minInvestAmountIsValid === DivPanelValidationTypes.Valid &&
      this.state.maxInvestAmountIsValid === DivPanelValidationTypes.Valid;

    const newVar = {isValid, ...result};
    this.props.handleChange(newVar);
  }

  monthlyIncomeAmountGoalHandleChange(evt: IRangeValidationResult) {
    const props = this.getProps();
    props.monthlyIncomeAmountGoal = evt.value;

    this.setState({
      monthlyIncomeAmountGoalIsValid: !evt.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(props);
    });
  }

  monthlySavedAmountHandleChange(evt: IRangeValidationResult) {
    const props = this.getProps();
    props.monthlySavedAmount = evt.value;
    this.setState({
      monthlySavedAmountIsValid: !evt.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(props);
    });
  }

  currentlySavedAmountHandleChange(evt: IRangeValidationResult) {
    const props = this.getProps();
    props.currentlySavedAmount = evt.value;

    this.setState({
      currentlySavedAmountIsValid: !evt.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(props);
    });
  }

  minMonthlyCashOnCashAmountHouseHandleChange(evt: IRangeValidationResult) {
    const props = this.getProps();
    props.minMonthlyCashOnCashAmountHouse = evt.value;

    this.setState({
      minMonthlyCashOnCashAmountHouseIsValid: !evt.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(props);
    });
  }

  minMonthlyCashOnCashAmountApartmentHandleChange(evt: IRangeValidationResult) {
    const props = this.getProps();
    props.minMonthlyCashOnCashAmountApartment = evt.value;

    this.setState({
      minMonthlyCashOnCashAmountApartmentIsValid: !evt.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(props);
    });
  }

  minEquityCapturePercentHandleChange(evt: IRangeValidationResult) {
    const props = this.getProps();
    props.minEquityCapturePercent = evt.value;

    this.setState({
      minEquityCapturePercentIsValid: !evt.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(props);
    });
  }

  minInvestAmountHandleChange(evt: IRangeValidationResult) {
    const props = this.getProps();
    props.minInvestAmount = evt.value;

    this.setState({
      minInvestAmountIsValid: !evt.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(props);
    });
  }

  maxInvestAmountHandleChange(evt: IRangeValidationResult) {
    const props = this.getProps();
    props.maxInvestAmount = evt.value;

    this.setState({
      maxInvestAmountIsValid: !evt.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(props);
    });
  }

  render() {
    return <DivWrap>
      <DivBackgroundImage/>
      <DivFlexContainerColumn>
        <PageTitleNegHeight>
          <PageTitle title='About You'/>
        </PageTitleNegHeight>
        <InputContainer title={''}
                        height={107}
                        backgroundColor={'transparent'}
                        inputContainerType={DivPanelValidationTypes.Invalid}
                        allInputContainerTypes={[this.state.monthlyIncomeAmountGoalIsValid]}>
          <GoalValidationInput title='Monthly Income Goal'
                               min={1000} max={1000000}
                               prefix='$'
                               value={this.props.userInput.monthlyIncomeAmountGoal}
                               onChange={this.monthlyIncomeAmountGoalHandleChange.bind(this)}/>
        </InputContainer>
        <InputContainer title='Savings Plan'
                        inputContainerType={DivPanelValidationTypes.Invalid}
                        allInputContainerTypes={[
                          this.state.monthlySavedAmountIsValid,
                          this.state.currentlySavedAmountIsValid]}>
          <RangeValidationInput title='Monthly Saved Amount'
                                min={1} max={1000000}
                                prefix='$'
                                value={this.props.userInput.monthlySavedAmount}
                                onChange={this.monthlySavedAmountHandleChange.bind(this)}/>

          <RangeValidationInput title='Currently Saved Amount'
                                min={0} max={1000000}
                                prefix='$'
                                value={this.props.userInput.currentlySavedAmount}
                                onChange={this.currentlySavedAmountHandleChange.bind(this)}/>
        </InputContainer>
        <InputContainer title='Rental Rules'
                        inputContainerType={DivPanelValidationTypes.Invalid}
                        allInputContainerTypes={[
                          this.state.minMonthlyCashOnCashAmountHouseIsValid,
                          this.state.minMonthlyCashOnCashAmountApartmentIsValid,
                          this.state.minEquityCapturePercentIsValid,
                          this.state.minEquityCapturePercentIsValid,
                          this.state.minInvestAmountIsValid,
                          this.state.maxInvestAmountIsValid]}>
          <RangeValidationInput title='Minimum SingleFamily Monthly Cash on Cash Amount'
                                min={1} max={10000}
                                prefix='$'
                                value={this.props.userInput.minMonthlyCashOnCashAmountHouse}
                                onChange={this.minMonthlyCashOnCashAmountHouseHandleChange.bind(this)}/>
          <RangeValidationInput title='Minimum Apartment Monthly Cash on Cash Amount'
                                min={1} max={10000}
                                prefix='$'
                                value={this.props.userInput.minMonthlyCashOnCashAmountApartment}
                                onChange={this.minMonthlyCashOnCashAmountApartmentHandleChange.bind(this)}/>

          <RangeValidationInput title='Minimum Equity Capture'
                                min={1} max={500}
                                suffix='%'
                                value={this.props.userInput.minEquityCapturePercent}
                                onChange={this.minEquityCapturePercentHandleChange.bind(this)}/>

          <RangeValidationInput title='Minimum Investment Amount'
                                min={0} max={1000000}
                                prefix='$'
                                value={this.props.userInput.minInvestAmount}
                                onChange={this.minInvestAmountHandleChange.bind(this)}/>

          <RangeValidationInput title='Maximum Investment Amount'
                                min={0} max={1000000}
                                prefix='$'
                                value={this.props.userInput.maxInvestAmount}
                                onChange={this.maxInvestAmountHandleChange.bind(this)}/>
        </InputContainer>
      </DivFlexContainerColumn>
    </DivWrap>;
  }
}

