import React from 'react';
import styled from 'styled-components';
import {mainTabConstants} from './constants/main-tab-constants';
import {DataStore} from './data-store';
import {IUserInputEntity} from './i-user-input-entity';
import {DivFlexContainer} from './components/div-flex-container';
import {IRentalGeneratorOptionEntity} from './i-rental-generator-option-entity';
import {GetStarted} from './get-started';
import {NavigationBar} from './components/nav-bar';
import {SpanIcon} from './components/span-icon';
import {UserInput} from './user-input';
import {IRentalContainerResult, RentalContainer} from './rental-container';
import {Simulator} from './simulator';
import {INavigationResponse} from './components/i-navigation-response';
import {userInputDefault} from './defaults/user-input-default';
import {rentalOptionsDefaults} from './defaults/rental-options-defaults';

const DATA_LOAD = 'load';
const DATA_CLEAN = 'do not load';
const DATA_DIRTY = 'dirty';
const DATA_SAVE_KEY = 'cubed-element-rental-retirement';

const GetStartedContainer = styled(DivFlexContainer)`
  width: 100%;
  align-items: center;
  flex-flow: column;
  justify-content: center;
  padding: 0 0 2em 0;
`;

interface IDataSave {
  userInput: IUserInputEntity;
  rentalOptionsInput: IRentalGeneratorOptionEntity[];
}

interface IGetDataSave {
  userInput: {
    value: IUserInputEntity
  };
  rentalOptionsInput: {
    value: IRentalGeneratorOptionEntity[]
  };
}

interface IGetDataToState {
  userInput: {
    value: IUserInputEntity,
    isValid: boolean
  };
  rentalOptionsInput: {
    value: IRentalGeneratorOptionEntity[],
    isValid: boolean
  };
}

export interface IMainState {
  load: string;
  doUpdate: boolean;
  tabName: string;
  userInput: {
    value?: IUserInputEntity,
    isValid: boolean
  };
  rentalOptionsInput: {
    value: IRentalGeneratorOptionEntity[],
    isValid: boolean
  }
}

export class Main extends React.Component<any, IMainState> {
  dataStore: DataStore;

  constructor(props) {
    super(props);
    this.dataStore = new DataStore();
    this.state = {
      load: DATA_LOAD,
      doUpdate: false,
      tabName: 'getStarted',
      userInput: {
        value: userInputDefault,
        isValid: false
      },
      rentalOptionsInput: {
        value: rentalOptionsDefaults,
        isValid: false
      }
    };
    this.handleNavClick = this.handleNavClick.bind(this);
    this.handleUserInputChange = this.handleUserInputChange.bind(this);
    if (!this.getData()) {
      this.saveData(this.state);
    }
  }

  getData(): IGetDataSave {
    const savedData: IDataSave = this.dataStore.get(DATA_SAVE_KEY);

    if (!savedData) {
      return null;
    }

    return {
      userInput: {
        value: savedData.userInput
      }, rentalOptionsInput: {
        value: savedData.rentalOptionsInput
      }
    };
  }

  saveData(currentState): void {
    const dataToSave: IDataSave = {
      userInput: currentState.userInput.value,
      rentalOptionsInput: currentState.rentalOptionsInput.value
    };
    this.dataStore.save(DATA_SAVE_KEY, dataToSave);
  }

  getTabName(newTabName): string {
    if (newTabName === mainTabConstants.rentalOptions) {
      return !this.state.userInput.isValid ? mainTabConstants.userInfo : newTabName;
    }

    if (newTabName === mainTabConstants.generate) {
      return !this.state.rentalOptionsInput.isValid ? mainTabConstants.rentalOptions : newTabName;
    }

    return newTabName;
  }

  handleNavClick(tabInfo: INavigationResponse): void {
    const adjustTabName = this.getTabName(tabInfo.tabName);
    this.setState(() => ({
      tabName: adjustTabName
    }));
  }

  handleUserInputChange(result): void {
    const curState = {...this.state};
    curState.load = DATA_DIRTY;
    curState.doUpdate = false;
    curState.userInput.value = result;
    curState.userInput.isValid = result.isValid;
    delete curState.userInput.value.isValid;
    this.setState(() => curState, () => {
      this.saveData(this.state);
    });
  }

  handleRentalGeneratorChange(result: IRentalContainerResult): void {
    const curState = {...this.state};
    curState.load = DATA_DIRTY;
    curState.doUpdate = false;
    curState.rentalOptionsInput.value = result.options;
    curState.rentalOptionsInput.isValid = result.isValid;
    this.setState(() => (curState), () => {
      this.saveData(this.state);
    });
  }

  componentDidMount() {
    if (this.state.load === DATA_LOAD) {
      const data: IGetDataSave = this.getData();

      const toState: IGetDataToState = {
        userInput: {
          isValid: this.state.userInput.isValid,
          value: data.userInput.value
        },
        rentalOptionsInput: {
          value: data.rentalOptionsInput.value,
          isValid: this.state.rentalOptionsInput.isValid
        }
      };

      this.setState({
        load: DATA_CLEAN,
        doUpdate: false,
        ...toState
      });
    }
  }

  render() {
    if (this.state.tabName === 'getStarted') {
      return <GetStarted handleClick={this.handleNavClick.bind(this)}/>;
    }

    return <GetStartedContainer id='get-started-flow' className={'mainContainerBlock'}>
      <NavigationBar handleClick={this.handleNavClick.bind(this)}
                     tabName={this.state.tabName}
                     tabs={Object.values(mainTabConstants).map(v => v)}
                     icon={<SpanIcon/>}
                     areActive={[
                       this.state.userInput.isValid,
                       this.state.rentalOptionsInput.isValid]}/>
      {this.state.tabName === mainTabConstants.userInfo && <div>
        <UserInput id='user-input' handleChange={this.handleUserInputChange.bind(this)}
                   userInput={this.state.userInput.value}/>
      </div>}
      {this.state.tabName === mainTabConstants.rentalOptions && <div>
        <RentalContainer id='rental-container'
                         handleChange={this.handleRentalGeneratorChange.bind(this)}
                         options={this.state.rentalOptionsInput.value}/>
      </div>}
      {this.state.tabName === mainTabConstants.generate &&
      <Simulator
        value={{userInput: this.state.userInput.value, rentalOptionsInput: this.state.rentalOptionsInput.value}}/>}
    </GetStartedContainer>;
  }
}

