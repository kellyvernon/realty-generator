import React from 'react';
import {RangeValidationInput} from './components/forms/range-validation-input';
import {currencyFormatter, fixedAndFloat, RentalTypes} from 'realty-generator-core';
import {InputContainer} from './components/input-container';
import {
  DivFlexContainer,
  DivFlexContainerAlignItems,
  DivFlexContainerDirections,
  DivFlexContainerJustifyContent
} from './components/div-flex-container';
import styled from 'styled-components';
import {CheckboxValidationInput} from './components/forms/checkbox-validation-input';
import {DivPanelValidationTypes} from './components/div-panel-validation';
import {DivInputContainer} from './components/div-Input-container';
import {Col, Row} from './components/row';
import {IRentalGeneratorOptionEntity} from './i-rental-generator-option-entity';
import {IChangeResult} from './components/forms/goal-validation-input';
import {rentalOptionRentalDefaults} from './defaults/rental-options-defaults';

const DivFlexContainerColumn = styled(DivFlexContainer).attrs(() => ({
  alignItems: DivFlexContainerAlignItems.CENTER,
  direction: DivFlexContainerDirections.COLUMN
}))`
  flex-flow: column;
  padding-left:40px;
`;

const CheckboxEnabled = styled(CheckboxValidationInput)`
  flex-grow: 1;
`;

const RowTransAction = styled(Row)`
  background-color: #1d1d26;
  border: 0 solid #254B50;border-bottom-width: 1px;

  padding-top: 10px;
  padding-bottom: 10px;
`;

export interface IRentalGeneratorOptionsProps {
  data: IRentalGeneratorOptionEntity;
  handleChange: (options: IRentalGeneratorOptionEntity[]) => void;
}

export interface IRentalGeneratorOptionsState {
  doLoad: boolean;
  renewalInMonthsIsValid: DivPanelValidationTypes;
  minRentalOpportunitiesIsValid: DivPanelValidationTypes;
  maxRentalOpportunitiesIsValid: DivPanelValidationTypes;
  lowestPriceDownIsValid: DivPanelValidationTypes;
  highestPriceDownIsValid: DivPanelValidationTypes;
  lowestSellPercentIsValid: DivPanelValidationTypes;
  highestSellPercentIsValid: DivPanelValidationTypes;
  lowestMinSellInYearsIsValid: DivPanelValidationTypes;
  highestMinSellInYearsIsValid: DivPanelValidationTypes;
  lowestCashOnCashPercentIsValid: DivPanelValidationTypes;
  highestCashOnCashPercentIsValid: DivPanelValidationTypes;
  lowestRefinancePercentIsValid: DivPanelValidationTypes;
  highestRefinancePercentIsValid: DivPanelValidationTypes;
  lowestMinRefinanceInYearsIsValid: DivPanelValidationTypes;
  highestMinRefinanceInYearsIsValid: DivPanelValidationTypes;
}

const getPrettyName = (value: string): string => {
  return value.replace(/([a-z])([A-Z])/g, '$1 $2');
};

export class RentalGeneratorOptions extends React.Component<IRentalGeneratorOptionsProps, IRentalGeneratorOptionsState> {
  static defaultProps = rentalOptionRentalDefaults;

  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.isEnabledChange = this.isEnabledChange.bind(this);
    this.renewalInMonthsHandleChange = this.renewalInMonthsHandleChange.bind(this);
    this.minRentalOpportunitiesHandleChange = this.minRentalOpportunitiesHandleChange.bind(this);
    this.maxRentalOpportunitiesHandleChange = this.maxRentalOpportunitiesHandleChange.bind(this);
    this.lowestPriceDownHandleChange = this.lowestPriceDownHandleChange.bind(this);
    this.highestPriceDownHandleChange = this.highestPriceDownHandleChange.bind(this);
    this.lowestSellPercentHandleChange = this.lowestSellPercentHandleChange.bind(this);
    this.highestSellPercentHandleChange = this.highestSellPercentHandleChange.bind(this);
    this.lowestMinSellInYearsHandleChange = this.lowestMinSellInYearsHandleChange.bind(this);
    this.highestMinSellInYearsHandleChange = this.highestMinSellInYearsHandleChange.bind(this);
    this.lowestCashOnCashPercentHandleChange = this.lowestCashOnCashPercentHandleChange.bind(this);
    this.highestCashOnCashPercentHandleChange = this.highestCashOnCashPercentHandleChange.bind(this);
    this.lowestMinRefinanceInYearsHandleChange = this.lowestMinRefinanceInYearsHandleChange.bind(this);
    this.highestMinRefinanceInYearsHandleChange = this.highestMinRefinanceInYearsHandleChange.bind(this);

    this.state = {
      doLoad: true,
      renewalInMonthsIsValid: DivPanelValidationTypes.Invalid,
      minRentalOpportunitiesIsValid: DivPanelValidationTypes.Invalid,
      maxRentalOpportunitiesIsValid: DivPanelValidationTypes.Invalid,
      lowestPriceDownIsValid: DivPanelValidationTypes.Invalid,
      highestPriceDownIsValid: DivPanelValidationTypes.Invalid,
      lowestSellPercentIsValid: DivPanelValidationTypes.Invalid,
      highestSellPercentIsValid: DivPanelValidationTypes.Invalid,
      lowestMinSellInYearsIsValid: DivPanelValidationTypes.Invalid,
      highestMinSellInYearsIsValid: DivPanelValidationTypes.Invalid,
      lowestCashOnCashPercentIsValid: DivPanelValidationTypes.Invalid,
      highestCashOnCashPercentIsValid: DivPanelValidationTypes.Invalid,
      lowestRefinancePercentIsValid: DivPanelValidationTypes.Invalid,
      highestRefinancePercentIsValid: DivPanelValidationTypes.Invalid,
      lowestMinRefinanceInYearsIsValid: DivPanelValidationTypes.Invalid,
      highestMinRefinanceInYearsIsValid: DivPanelValidationTypes.Invalid
    };
  }

  getProps(): IRentalGeneratorOptionEntity {
    return {...this.props.data};
  }

  isEnabledChange(target: IChangeResult<boolean>) {
    const result = this.getProps();
    result.isEnabled = target.value;
    this.handleChange(result);
  }

  renewalInMonthsHandleChange(target: IChangeResult<number>) {
    const result = this.getProps();
    result.renewalInMonths = target.value;

    this.setState({
      renewalInMonthsIsValid: !target.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(result);
    });
  }

  minRentalOpportunitiesHandleChange(target: IChangeResult<number>) {
    const result = this.getProps();
    result.minRentalOpportunities = target.value;

    this.setState({
      minRentalOpportunitiesIsValid: !target.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(result);
    });
  }

  maxRentalOpportunitiesHandleChange(target: IChangeResult<number>) {
    const result = this.getProps();
    result.maxRentalOpportunities = target.value;

    this.setState({
      maxRentalOpportunitiesIsValid: !target.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(result);
    });
  }

  lowestPriceDownHandleChange(target: IChangeResult<number>) {
    const result = this.getProps();
    result.lowestPriceDown = target.value;

    this.setState({
      lowestPriceDownIsValid: !target.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(result);
    });
  }

  highestPriceDownHandleChange(target: IChangeResult<number>) {
    const result = this.getProps();
    result.highestPriceDown = target.value;

    this.setState({
      highestPriceDownIsValid: !target.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(result);
    });
  }

  lowestSellPercentHandleChange(target: IChangeResult<number>) {
    const result = this.getProps();
    result.lowestSellPercent = target.value;

    this.setState({
      lowestSellPercentIsValid: !target.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(result);
    });
  }

  highestSellPercentHandleChange(target: IChangeResult<number>) {
    const result = this.getProps();
    result.highestSellPercent = target.value;

    this.setState({
      highestSellPercentIsValid: !target.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(result);
    });
  }

  lowestMinSellInYearsHandleChange(target: IChangeResult<number>) {
    const result = this.getProps();
    result.lowestMinSellInYears = target.value;

    this.setState({
      lowestMinSellInYearsIsValid: !target.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(result);
    });
  }

  highestMinSellInYearsHandleChange(target: IChangeResult<number>) {
    const result = this.getProps();
    result.highestMinSellInYears = target.value;

    this.setState({
      highestMinSellInYearsIsValid: !target.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(result);
    });
  }

  lowestMinRefinanceInYearsHandleChange(target: IChangeResult<number>) {
    const result = this.getProps();
    result.lowestMinRefinanceInMonths = target.value;

    this.setState({
      lowestMinRefinanceInYearsIsValid: !target.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(result);
    });
  }

  highestMinRefinanceInYearsHandleChange(target: IChangeResult<number>) {
    const result = this.getProps();
    result.highestMinRefinanceInMonths = target.value;

    this.setState({
      highestMinRefinanceInYearsIsValid: !target.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(result);
    });
  }

  lowestCashOnCashPercentHandleChange(target: IChangeResult<number>) {
    const result = this.getProps();
    result.lowestCashOnCashPercent = target.value;

    this.setState({
      lowestCashOnCashPercentIsValid: !target.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(result);
    });
  }

  highestCashOnCashPercentHandleChange(target: IChangeResult<number>) {
    const result = this.getProps();
    result.highestCashOnCashPercent = target.value;

    this.setState({
      highestCashOnCashPercentIsValid: !target.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(result);
    });
  }

  lowestRefinancePercentHandleChange(target: IChangeResult<number>) {
    const result = this.getProps();
    result.lowestRefinancePercent = target.value;

    this.setState({
      lowestRefinancePercentIsValid: !target.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(result);
    });
  }

  highestRefinancePercentHandleChange(target: IChangeResult<number>) {
    const result = this.getProps();
    result.highestRefinancePercent = target.value;

    this.setState({
      highestRefinancePercentIsValid: !target.isValid ? DivPanelValidationTypes.Invalid : DivPanelValidationTypes.Valid
    }, () => {
      this.handleChange(result);
    });
  }

  handleChange(result) {
    result.isValid = !result.isEnabled ? true :
      this.state.renewalInMonthsIsValid === DivPanelValidationTypes.Valid &&
      this.state.minRentalOpportunitiesIsValid === DivPanelValidationTypes.Valid &&
      this.state.maxRentalOpportunitiesIsValid === DivPanelValidationTypes.Valid &&
      this.state.lowestPriceDownIsValid === DivPanelValidationTypes.Valid &&
      this.state.highestPriceDownIsValid === DivPanelValidationTypes.Valid &&
      this.state.lowestSellPercentIsValid === DivPanelValidationTypes.Valid &&
      this.state.highestSellPercentIsValid === DivPanelValidationTypes.Valid &&
      this.state.lowestMinSellInYearsIsValid === DivPanelValidationTypes.Valid &&
      this.state.highestMinSellInYearsIsValid === DivPanelValidationTypes.Valid &&
      this.state.highestMinSellInYearsIsValid === DivPanelValidationTypes.Valid &&
      this.state.lowestMinRefinanceInYearsIsValid === DivPanelValidationTypes.Valid &&
      this.state.highestMinRefinanceInYearsIsValid === DivPanelValidationTypes.Valid &&
      this.state.lowestCashOnCashPercentIsValid === DivPanelValidationTypes.Valid &&
      this.state.highestCashOnCashPercentIsValid === DivPanelValidationTypes.Valid &&
      this.state.lowestRefinancePercentIsValid === DivPanelValidationTypes.Valid &&
      this.state.highestRefinancePercentIsValid === DivPanelValidationTypes.Valid;
    this.props.handleChange(result);
  }

  render() {
    const ranges = {
      investmentAmounts: this.props.data.type === RentalTypes.PassiveApartment
        ? [50000, 100000, 150000, 200000, 250000, 500000]
        : [20000, 30000, 40000],

      minRenewalInMonths: 1,
      maxRenewalInMonths: this.props.data.type === RentalTypes.PassiveApartment ? 4 : 6,

      minRentalOpportunities: 1,
      maxRentalOpportunities: this.props.data.type === RentalTypes.PassiveApartment ? 20 : 10,

      minPriceDown: this.props.data.type === RentalTypes.PassiveApartment ? 250000 : 25000,
      maxPriceDown: this.props.data.type === RentalTypes.PassiveApartment ? 50000000 : 400000,

      minSellPercent: this.props.data.type === RentalTypes.PassiveApartment ? 60 : 60,
      maxSellPercent: this.props.data.type === RentalTypes.PassiveApartment ? 500 : 140,

      minSellInYears: this.props.data.type === RentalTypes.PassiveApartment ? 5 : 1,
      maxSellInYears: this.props.data.type === RentalTypes.PassiveApartment ? 12 : 4,

      minRefinanceInMonths: this.props.data.type === RentalTypes.PassiveApartment ? 24 : 1,
      maxRefinanceInMonths: this.props.data.type === RentalTypes.PassiveApartment ? 48 : 4,

      minCashOnCashPercent: this.props.data.type === RentalTypes.PassiveApartment ? 5 : 1,
      maxCashOnCashPercent: this.props.data.type === RentalTypes.PassiveApartment ? 15 : 20,

      minRefinancePercent: this.props.data.type === RentalTypes.PassiveApartment ? 25 : 15,
      maxRefinancePercent: this.props.data.type === RentalTypes.PassiveApartment ? 100 : 25,

      minInvestmentPercent: this.props.data.type === RentalTypes.PassiveApartment ? 1 : 10,
      maxInvestmentPercent: this.props.data.type === RentalTypes.PassiveApartment ? 10 : 33
    };

    const paCashOnCashes = ranges.investmentAmounts.map(e => {
      const lowestAnnualAmount = this.props.data.lowestCashOnCashPercent * e / 100;
      const highestAnnualAmount = this.props.data.highestCashOnCashPercent * e / 100;
      return ({
        investmentAmount: currencyFormatter(e),
        lowestAnnualAmount: currencyFormatter(lowestAnnualAmount),
        lowestAmountMonthly: currencyFormatter(lowestAnnualAmount / 12),
        highestAnnualAmount: currencyFormatter(highestAnnualAmount),
        highestAmountMonthly: currencyFormatter(highestAnnualAmount / 12)
      });
    }).map((am, index) => {
      return <RowTransAction flexGrow={1} key={index}>
        <Col flexGrow={1} className='text-right'>{am.investmentAmount}</Col>
        <Col flexGrow={1} className='text-right'>{am.lowestAnnualAmount}</Col>
        <Col flexGrow={1} className='text-right'>{am.lowestAmountMonthly}</Col>
        <Col flexGrow={1} className='text-right'>{am.highestAnnualAmount}</Col>
        <Col flexGrow={1} className='text-right'>{am.highestAmountMonthly}</Col>
      </RowTransAction>;
    });

    const sfCashOnCashes = ranges.investmentAmounts.map(e => {
      const lowestInvestedPercent = e / this.props.data.lowestPriceDown * 100;
      const highestInvestedPercent = e / this.props.data.highestPriceDown * 100;
      const lowestCashFlow = e * this.props.data.lowestCashOnCashPercent / 100;
      const highestCashFlow = e * this.props.data.highestCashOnCashPercent / 100;
      return ({
        investmentAmount: currencyFormatter(e),
        lowestMarketPrice: currencyFormatter(this.props.data.lowestPriceDown),
        lowestInvestedPercent: fixedAndFloat(lowestInvestedPercent),
        lowestCashFlow: currencyFormatter(lowestCashFlow),
        lowestCashFlowMonthly: currencyFormatter(lowestCashFlow / 12),
        highestMarketPrice: currencyFormatter(this.props.data.highestPriceDown),
        highestInvestedPercent: fixedAndFloat(highestInvestedPercent),
        highestCashFlow: currencyFormatter(highestCashFlow),
        highestCashFlowMonthly: currencyFormatter(highestCashFlow / 12),
      });
    }).map((am, index) => {
      return <RowTransAction flexGrow={1} key={index}>
        <Col flexGrow={1} className='text-right'>{am.investmentAmount}</Col>
        <Col flexGrow={1} className='text-right'>{am.lowestCashFlow}</Col>
        <Col flexGrow={1} className='text-right'>{am.lowestCashFlowMonthly}</Col>
        <Col flexGrow={1} className='text-right'>{am.highestCashFlow}</Col>
        <Col flexGrow={1} className='text-right'>{am.highestCashFlowMonthly}</Col>
      </RowTransAction>;
    });

    const aptSalesRange = ranges.investmentAmounts.map(e => {
      const lowest = fixedAndFloat((this.props.data.lowestSellPercent * e / 100), 2);
      const highest = fixedAndFloat((this.props.data.highestSellPercent * e / 100), 2);

      return {
        amount: currencyFormatter(e),
        lowestSellPercentAmount: currencyFormatter(lowest),
        highestSellPercentAmount: currencyFormatter(highest)
      };
    }).map((am, index) => {
      return <RowTransAction flexGrow={1} key={index}>
        <Col flexGrow={1} className='text-right'>{am.amount}</Col>
        <Col flexGrow={1} className='text-right'>{am.lowestSellPercentAmount}</Col>
        <Col flexGrow={1} className='text-right'>{am.highestSellPercentAmount}</Col>
      </RowTransAction>;
    });

    const houseSalesRange = [this.props.data.lowestPriceDown || 0, this.props.data.highestPriceDown || 0].map(e => {
      return {
        amount: currencyFormatter(e),
        lowestSellPercentAmount: currencyFormatter(e * this.props.data.lowestSellPercent / 100),
        highestSellPercentAmount: currencyFormatter(e * this.props.data.highestSellPercent / 100)
      };
    }).map((am, index) => {
      return <RowTransAction flexGrow={1} key={index}>
        <Col flexGrow={1} className='text-right'>{am.amount}</Col>
        <Col flexGrow={1} className='text-right'>{am.lowestSellPercentAmount}</Col>
        <Col flexGrow={1} className='text-right'>{am.highestSellPercentAmount}</Col>
      </RowTransAction>;
    });

    const refiRanges = ranges.investmentAmounts.map(e => {
      const lowest = this.props.data.lowestRefinancePercent ? fixedAndFloat((this.props.data.lowestRefinancePercent * e / 100), 2) : 0;
      const highest = this.props.data.highestRefinancePercent ? fixedAndFloat((this.props.data.highestRefinancePercent * e / 100), 2) : 0;

      return {
        amount: currencyFormatter(e),
        lowestRefinancePercentAmount: currencyFormatter(lowest),
        highestRefinancePercentAmount: currencyFormatter(highest)
      };
    });

    const imageValue = (t: RentalTypes): string => t === RentalTypes.PassiveApartment ? 'apartment' : 'house';

    return <DivFlexContainerColumn>
      <DivInputContainer direction={DivFlexContainerDirections.ROW} alignItems={DivFlexContainerAlignItems.CENTER}
                         justifyContent={DivFlexContainerJustifyContent.SPACE_BETWEEN}>
        <CheckboxEnabled title={`Generate ${getPrettyName(RentalTypes[this.props.data.type])}`}
                         value={this.props.data.isEnabled}
                         onChange={this.isEnabledChange.bind(this)}/>
        <img src={`images/${imageValue(this.props.data.type)}.jpg`}
             width='498'
             height='426' alt={imageValue(this.props.data.type)}/>
      </DivInputContainer>
      <InputContainer title={'Life Span'}
                      inputContainerType={DivPanelValidationTypes.Invalid}
                      allInputContainerTypes={[
                        this.state.renewalInMonthsIsValid,
                        this.state.maxRentalOpportunitiesIsValid,
                        this.state.maxRentalOpportunitiesIsValid]}>
        <RangeValidationInput title='Length Deal is Available'
                              min={ranges.minRenewalInMonths} max={ranges.maxRenewalInMonths}
                              suffix='Months'
                              isEnabled={this.props.data.isEnabled}
                              value={this.props.data.renewalInMonths}
                              onChange={this.renewalInMonthsHandleChange.bind(this)}/>

        <RangeValidationInput title='Minimum Generated Rental Opportunities'
                              min={ranges.minRentalOpportunities} max={ranges.maxRentalOpportunities}
                              value={this.props.data.minRentalOpportunities}
                              isEnabled={this.props.data.isEnabled}
                              onChange={this.minRentalOpportunitiesHandleChange.bind(this)}/>

        <RangeValidationInput title='Maximum Generated Rental Opportunities'
                              min={ranges.minRentalOpportunities} max={ranges.maxRentalOpportunities}
                              value={this.props.data.maxRentalOpportunities}
                              isEnabled={this.props.data.isEnabled}
                              onChange={this.maxRentalOpportunitiesHandleChange.bind(this)}/>
      </InputContainer>
      <InputContainer title={'Market Value'}
                      inputContainerType={DivPanelValidationTypes.Invalid}
                      allInputContainerTypes={[this.state.lowestPriceDownIsValid, this.state.highestPriceDownIsValid]}>
        <RangeValidationInput title='Lowest Market Value'
                              min={ranges.minPriceDown} max={ranges.maxPriceDown} prefix='$'
                              value={this.props.data.lowestPriceDown}
                              isEnabled={this.props.data.isEnabled}
                              onChange={this.lowestPriceDownHandleChange.bind(this)}/>

        <RangeValidationInput title='Highest Market Value'
                              min={ranges.minPriceDown} max={ranges.maxPriceDown} prefix='$'
                              value={this.props.data.highestPriceDown}
                              isEnabled={this.props.data.isEnabled}
                              onChange={this.highestPriceDownHandleChange.bind(this)}/>
      </InputContainer>
      <InputContainer title={'Sell Price'}
                      inputContainerType={DivPanelValidationTypes.Invalid}
                      allInputContainerTypes={[
                        this.state.lowestSellPercentIsValid,
                        this.state.highestSellPercentIsValid,
                        this.state.lowestMinSellInYearsIsValid,
                        this.state.highestMinSellInYearsIsValid]}>
        <RangeValidationInput title='Lowest Minimum Sell'
                              min={ranges.minSellInYears} max={ranges.maxSellInYears} suffix='years'
                              value={this.props.data.lowestMinSellInYears}
                              isEnabled={this.props.data.isEnabled}
                              onChange={this.lowestMinSellInYearsHandleChange.bind(this)}/>

        <RangeValidationInput title='Highest Minimum Sell'
                              min={ranges.minSellInYears} max={ranges.maxSellInYears} suffix='years'
                              value={this.props.data.highestMinSellInYears}
                              isEnabled={this.props.data.isEnabled}
                              onChange={this.highestMinSellInYearsHandleChange.bind(this)}/>

        <RangeValidationInput title='Lowest Sell'
                              min={ranges.minSellPercent} max={ranges.maxSellPercent} suffix='%'
                              value={this.props.data.lowestSellPercent}
                              isEnabled={this.props.data.isEnabled}
                              onChange={this.lowestSellPercentHandleChange.bind(this)}/>

        <RangeValidationInput title='Highest Sell'
                              min={ranges.minSellPercent} max={ranges.maxSellPercent} suffix='%'
                              value={this.props.data.highestSellPercent}
                              isEnabled={this.props.data.isEnabled}
                              onChange={this.highestSellPercentHandleChange.bind(this)}/>

        <div>
          <RowTransAction flexGrow={1}>
            {this.props.data.type === RentalTypes.PassiveApartment &&
            <Col flexGrow={1}><strong>Investment Amount</strong></Col>}
            {this.props.data.type === RentalTypes.SingleFamily &&
            <Col flexGrow={1}><strong>Market Amount</strong></Col>}
            <Col flexGrow={1}><strong>Lowest Sell</strong></Col>
            <Col flexGrow={1}><strong>Highest Sell</strong></Col>
          </RowTransAction>
          {this.props.data.type === RentalTypes.PassiveApartment && aptSalesRange}
          {this.props.data.type === RentalTypes.SingleFamily && houseSalesRange}
        </div>
      </InputContainer>
      <InputContainer title={'Cash Flow'}
                      inputContainerType={DivPanelValidationTypes.Invalid}
                      allInputContainerTypes={[
                        this.state.lowestCashOnCashPercentIsValid,
                        this.state.highestCashOnCashPercentIsValid]}>
        <RangeValidationInput title='Lowest Cash on Cash'
                              min={ranges.minCashOnCashPercent} max={ranges.maxCashOnCashPercent} suffix='%'
                              value={this.props.data.lowestCashOnCashPercent}
                              isEnabled={this.props.data.isEnabled}
                              onChange={this.lowestCashOnCashPercentHandleChange.bind(this)}/>

        <RangeValidationInput title='Highest Cash on Cash'
                              min={ranges.minCashOnCashPercent} max={ranges.maxCashOnCashPercent} suffix='%'
                              value={this.props.data.highestCashOnCashPercent}
                              isEnabled={this.props.data.isEnabled}
                              onChange={this.highestCashOnCashPercentHandleChange.bind(this)}/>

        {this.props.data.type === RentalTypes.PassiveApartment && <div>
          <RowTransAction flexGrow={1}>
            <Col flexGrow={1}><strong>Investment Amount</strong></Col>
            <Col flexGrow={1}><strong>Lowest Annual CashFlow</strong></Col>
            <Col flexGrow={1}><strong>Lowest Mo CashFlow</strong></Col>
            <Col flexGrow={1}><strong>Highest Annual CashFlow</strong></Col>
            <Col flexGrow={1}><strong>Highest Mo CashFlow</strong></Col>
          </RowTransAction>
          {paCashOnCashes}
        </div>}
        {this.props.data.type === RentalTypes.SingleFamily && <div>
          <RowTransAction flexGrow={1}>
            <Col flexGrow={1}><strong>Investment Amount</strong></Col>
            <Col flexGrow={1}><strong>Lowest Annual CashFlow</strong></Col>
            <Col flexGrow={1}><strong>Lowest Mo CashFlow</strong></Col>
            <Col flexGrow={1}><strong>Highest Annual CashFlow</strong></Col>
            <Col flexGrow={1}><strong>Highest Mo CashFlow</strong></Col>
          </RowTransAction>
          {sfCashOnCashes}
        </div>}

      </InputContainer>
      <InputContainer title={'Refinance'}
                      inputContainerType={DivPanelValidationTypes.Invalid}
                      allInputContainerTypes={[
                        this.state.lowestRefinancePercentIsValid,
                        this.state.highestRefinancePercentIsValid,
                        this.state.lowestMinRefinanceInYearsIsValid,
                        this.state.highestMinRefinanceInYearsIsValid]}>
        <RangeValidationInput title='Lowest Minimum Refinance'
                              min={ranges.minRefinanceInMonths} max={ranges.maxRefinanceInMonths} suffix='months'
                              value={this.props.data.lowestMinRefinanceInMonths}
                              isEnabled={this.props.data.isEnabled}
                              onChange={this.lowestMinRefinanceInYearsHandleChange.bind(this)}/>

        <RangeValidationInput title='Highest Minimum Refinance'
                              min={ranges.minRefinanceInMonths} max={ranges.maxRefinanceInMonths} suffix='months'
                              value={this.props.data.highestMinRefinanceInMonths}
                              isEnabled={this.props.data.isEnabled}
                              onChange={this.highestMinRefinanceInYearsHandleChange.bind(this)}/>

        <RangeValidationInput title='Lowest Refinance'
                              min={ranges.minRefinancePercent} max={ranges.maxRefinancePercent} suffix='%'
                              value={this.props.data.lowestRefinancePercent}
                              isEnabled={this.props.data.isEnabled}
                              onChange={this.lowestRefinancePercentHandleChange.bind(this)}/>

        <RangeValidationInput title='Highest Refinance'
                              min={ranges.minRefinancePercent} max={ranges.maxRefinancePercent} suffix='%'
                              value={this.props.data.highestRefinancePercent}
                              isEnabled={this.props.data.isEnabled}
                              onChange={this.highestRefinancePercentHandleChange.bind(this)}/>

        <div>
          <RowTransAction flexGrow={1}>
            <Col flexGrow={1}><strong>Market Price</strong></Col>
            <Col flexGrow={1}><strong>Lowest Refi</strong></Col>
            <Col flexGrow={1}><strong>Highest Refi</strong></Col>
          </RowTransAction> {
          refiRanges.map((am, index) => {
            return <RowTransAction flexGrow={1} key={index}>
              <Col flexGrow={1} className='text-right'>{am.amount}</Col>
              <Col flexGrow={1} className='text-right'>{am.lowestRefinancePercentAmount}</Col>
              <Col flexGrow={1} className='text-right'>{am.lowestRefinancePercentAmount}</Col>
            </RowTransAction>;
          })
        } </div>
      </InputContainer>
    </DivFlexContainerColumn>;
  }
}
