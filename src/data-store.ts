'use strict';

import bs from 'browser-storage';

export class DataStore {
  localStorage: any;

  constructor() {
    this.localStorage = bs;
  }

  save(name: string, entity: any) {
    this.localStorage.setItem(name, JSON.stringify(entity));
  }

  get(name): any | null {
    const item = this.localStorage.getItem(name);

    if (item === undefined || item === null) {
      return null;
    }

    return JSON.parse(item);
  }

  delete(name: string) {
    this.localStorage.removeItem(name);
  }
}
