import React from 'react';
import {Form} from './components/forms/form';
import {Legend} from './components/forms/legend';
import {Fieldset} from './components/forms/field-set';
import {Label} from './components/forms/label';
import {Input} from './components/forms/input';

export class SingleFamilyInput extends React.Component {
  render() {
    return <Form>

      <Legend>Single Family Input:</Legend>
      <Fieldset>
        <Label htmlFor="renewalInMonths">Length Deal is Available</Label>
        <Input type="number" name="number" id="renewalInMonths" placeholder="1000"/>
      </Fieldset>
      <Fieldset>
        <Label htmlFor="maxRentalOpportunities">Maximum Generated Rental Opportunities</Label>
        <Input type="number" name="number" id="maxRentalOpportunities" placeholder="100"/>
      </Fieldset>
      <Fieldset>
        <Label htmlFor="lowestPriceDown">Lowest Price Down</Label>
        <Input type="number" name="number" id="lowestPriceDown" placeholder="5"/>
      </Fieldset>
      <Fieldset>
        <Label htmlFor="highestPriceDown">Highest Price Down</Label>
        <Input type="number" name="number" id="highestPriceDown" placeholder="5"/>
      </Fieldset>
      <Fieldset>
        <Label htmlFor="lowestSellPrice">Lowest Sell Price</Label>
        <Input type="number" name="number" id="lowestSellPrice" placeholder="50000"/>
      </Fieldset>
      <Fieldset>
        <Label htmlFor="highestSellPrice">Highest Sell Price</Label>
        <Input type="number" name="number" id="highestSellPrice" placeholder="100000"/>
      </Fieldset>
      <Fieldset>
        <Label htmlFor="lowestMinSellInYears">Lowest Minimum Sell in Years</Label>
        <Input type="number" name="number" id="lowestMinSellInYears" placeholder="100000"/>
      </Fieldset>
      <Fieldset>
        <Label htmlFor="highestMinSellInYears">Highest Minimum Sell in Years</Label>
        <Input type="number" name="number" id="highestMinSellInYears" placeholder="100000"/>
      </Fieldset>
      <Fieldset>
        <Label htmlFor="lowestCashOnCashPercent">Lowest Cash on Cash Percent</Label>
        <Input type="number" name="number" id="lowestCashOnCashPercent" placeholder="100000"/>
      </Fieldset>
      <Fieldset>
        <Label htmlFor="highestCashOnCashPercent">Highest Cash on Cash Percent</Label>
        <Input type="number" name="number" id="highestCashOnCashPercent" placeholder="100000"/>
      </Fieldset>
      <Fieldset>
        <Label htmlFor="lowestInvestmentPercent">Lowest Investment Percent</Label>
        <Input type="number" name="number" id="lowestInvestmentPercent" placeholder="100000"/>
      </Fieldset>
      <Fieldset>
        <Label htmlFor="highestInvestmentPercent">Highest Investment Percent</Label>
        <Input type="number" name="number" id="highestInvestmentPercent" placeholder="100000"/>
      </Fieldset>
    </Form>;
  }
}
